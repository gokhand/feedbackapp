export const environment = {
  production: true,
  isDev: false,
  api: 'xyz.com/api',
  adalClientId: '008bd943-e4ab-40b9-99ec-b44e530d8c60',
  adalAudience: 'https://localhost:44666',
  googleAnalyticsId: 'UA-XXXXXX-X'
};
