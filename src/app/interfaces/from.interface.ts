export interface From {
  hasPhoto?: boolean;
  peopleFinderId?: number;
  fullName?: string;
  title?: string;
  dept?: string;
}
