interface Employee {
  hasPhoto: boolean;
  id: string;
  peopleFinderId?: number;
  fullName: string;
  title: string;
}

export interface Feedback {
  employee: Employee;
  threadId: number;
  message: string;
  status: string;
  dateCreated: string;
  unreadCount: number;
  isRead?: boolean;
}
