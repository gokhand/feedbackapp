export interface Recipient {
  id?: number;
  ntId?: string;
  peopleFinderId?: number;
  dept?: string;
  fullName?: string;
  title?: string;
  login?: string;
}
