export interface Notifications {
  givenUpdate?: number;
  receivedUpdate?: number;
}
