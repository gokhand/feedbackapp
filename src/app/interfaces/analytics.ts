export interface Analytics {
    category?: string;
    action?: string;
    label?: string;
    value?: number;
}
