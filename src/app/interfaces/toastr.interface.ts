export interface ToastrConfig {
  message: string;
  type: string;
  timeout?: number;
}
