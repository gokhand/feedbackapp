export interface Myuser {
  peopleFinderId?: number;
  fullName?: string;
  title?: string;
  ntId?: string;
  id?: number;
  hasAgreedPrivacy?: boolean;
  hasAgreedLegal?: boolean;
  hasPhoto?: boolean;
}
