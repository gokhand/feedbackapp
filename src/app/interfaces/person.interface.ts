export interface Person {
  peopleFinderId?: number;
  id?: number;
  ntId?: string;
  hasPhoto: boolean;
  fullName: string;
  title: string;
  dept: string;
  login: string;
}
