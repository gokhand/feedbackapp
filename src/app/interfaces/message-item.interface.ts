export interface MessageItem {
  threadId?: number;
  isMine?: boolean;
  givingPeopleFinderId?: number;
  receivingPeopleFinderId?: number;
  message?: string;
  dateCreated?: string;
}
