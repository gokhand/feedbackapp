export interface Correspondent {
  hasPhoto?: boolean;
  peopleFinderId?: number;
  id?: number;
  fullName?: string;
  title?: string;
  dept?: string;
}
