import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { PubSubModule } from 'angular2-pubsub';
import { RouterModule, Routes } from '@angular/router';

import { AdalconfigService } from './services/adalconfig.service';
import { AdalService } from 'ng2-adal/core';
import { AppService } from './app.service';
import { PersonService } from './services/person.service';
import { SessionService } from './services/session.service';

import { HttpInterceptorServiceFactoryProvider } from './services/http-interceptor.service';

import { AboutOverlayModule } from './components/about-overlay/about-overlay.module';
import { ActionsComponent } from './components/actions/actions.component';
import { AccountMenuModule } from './components/account-menu/account-menu.module';
import { AnalyticsModule } from './components/analytics/analytics.module';
import { AppComponent } from './app.component';
import { AuthSuccessComponent } from './components/auth-success/auth-success.component';
import { BrandingComponent } from './components/branding/branding.component';
import { ComposeOverlayModule } from './components/compose-overlay/compose-overlay.module';
import { DirectivesModule } from './directives/directives.module';
import { LegalOverlayModule } from './components/legal-overlay/legal-overlay.module';
import { PrivacyOverlayModule } from './components/privacy-overlay/privacy-overlay.module';
import { SearchOverlayModule } from './components/search-overlay/search-overlay.module';
import { TabsComponent } from './components/tabs/tabs.component';
import { TipsOverlayModule } from './components/tips-overlay/tips-overlay.module';
import { ToastrModule } from './components/toastr/toastr.module';
import { ThreadOverlayModule } from './components/thread-overlay/thread-overlay.module';

const routes: Routes = [
  { path: '', redirectTo: 'feedback/received', pathMatch: 'full'},
  { path: 'dashboard', redirectTo: 'feedback/received'},
  { path: 'auth', loadChildren: 'app/features/auth/auth.module#AuthModule'},
  { path: 'thread', loadChildren: 'app/features/thread/thread.module#ThreadModule' },
  {
    path: 'feedback',
    loadChildren: 'app/features/list-feedback/list-feedback.module#ListFeedbackModule'
  },
  {
    path: 'search',
    loadChildren: 'app/features/search-people/search-people.module#SearchPeopleModule'
  },
  { path: ':id_token', component: AuthSuccessComponent }
];

@NgModule({
  imports: [
    AboutOverlayModule,
    AccountMenuModule,
    AnalyticsModule,
    BrowserModule,
    ComposeOverlayModule,
    DirectivesModule,
    FormsModule,
    HttpModule,
    LegalOverlayModule,
    PubSubModule.forRoot(),
    PrivacyOverlayModule,
    RouterModule.forRoot(routes, { useHash: true }),
    SearchOverlayModule,
    ThreadOverlayModule,
    TipsOverlayModule,
    ToastrModule
  ],
  declarations: [
    ActionsComponent,
    AppComponent,
    AuthSuccessComponent,
    BrandingComponent,
    TabsComponent
  ],
  exports: [

  ],
  providers: [
    AdalService,
    AdalconfigService,
    AppService,
    HttpInterceptorServiceFactoryProvider,
    PersonService,
    SessionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
