import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonService } from '../../../services/person.service';
import { Person } from '../../../interfaces/person.interface';

@Component({
  selector: 'app-get',
  templateUrl: './get.component.html',
  styleUrls: ['./get.component.scss']
})
export class GetComponent implements OnInit {

  constructor(private router: Router, private service: PersonService) { }

  ngOnInit() {
  }

  public personPicked(person: Person): void {
    this.service.setPerson(person);
    this.router.navigate(['thread/new', 'request']);
  }

}
