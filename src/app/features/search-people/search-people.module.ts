import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { LookupModule } from '../../components/lookup/lookup.module';

import { LoginGuard } from '../../guards/login.guard';

import { GiveComponent } from './give/give.component';
import { GetComponent } from './get/get.component';


const routes: Routes = [
  { path: 'give', component: GiveComponent, canActivate:[LoginGuard], data: { name: 'giveFeedback' } },
  { path: 'request', component: GetComponent, canActivate:[LoginGuard], data: { name: 'getFeedback' } },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LookupModule
  ],
  declarations: [
    GiveComponent,
    GetComponent
  ],
  providers: [
    LoginGuard
  ]
})
export class SearchPeopleModule { }
