import { Component } from '@angular/core';

import { AdalService } from 'ng2-adal/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  constructor( private adalService: AdalService) { }

  login() {
    this.adalService.login();
  }

}
