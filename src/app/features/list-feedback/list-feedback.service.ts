import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ListFeedbackService {

  constructor( private http: Http ) { }

  public getFeedbacks(resource, page, pageSize): Observable<object> {
    return this.http.get(`${resource}?page=${page}&pageSize=${pageSize}`).map((res: Response) => {
      return res;
    });
  }

  public test(): Observable<object> {
    return this.http.get('test').map((res: Response) => {
      return res;
    });
  }

  public sortByDate(_array): string[] {
    _array.sort((prev, next) => {
      return new Date(next.dateCreated).getTime() - new Date(prev.dateCreated).getTime();
    });
    return _array;
  }


}
