import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListFeedbackService } from '../list-feedback.service';
import { PubSubService } from 'angular2-pubsub';
import { config } from '../../../app.config';
import { uniqWith, isEqual, includes as _includes } from 'lodash';

@Component({
  selector: 'app-split',
  templateUrl: './split.component.html',
  styleUrls: ['./split.component.scss']
})
export class SplitComponent implements OnInit, OnDestroy {

  public feedbacksReceived: Array<any> = [];
  public feedbacksGiven: Array<any> = [];

  private receivedSubscription: any;
  private givenSubscription: any;
  private scrollSubscription: any;
  private statusSubscription: any;
  private updateSubscription: any;
  public isFetching: boolean;
  private seenThreads: number[] = [];
  private scrollIndex = 0;
  private resultsPerPage: number = config.feedbacks.resultsPerPage;

  constructor(private service: ListFeedbackService, private pubSub: PubSubService) {

    this.scrollSubscription = this.pubSub.$sub('EVT_ELEM_SCROLL_END').subscribe((state) => {
      console.log('scroll fired');
      this.scrollIndex++;
      this.getReceivedFeedback(this.scrollIndex);
      this.getGivenFeedback(this.scrollIndex);
    });

    this.statusSubscription = this.pubSub.$sub('EVT_THREAD_SEEN').subscribe((threadId) => {
      if(!_includes(this.seenThreads, threadId)){
        //this.seenThreads.push(threadId);
        //this.updateStatuses();
      }
    });

    this.updateSubscription = this.pubSub.$sub('EVT_NEW_FEEDBACK').subscribe((type: string) => {
      this.getGivenFeedback(0);
      this.getReceivedFeedback(0);
    });

  }

  ngOnInit() {
    this.scrollIndex = 0;
    this.getGivenFeedback(0);
    this.getReceivedFeedback(0);
  }
  ngOnDestroy() {
    if (!!this.receivedSubscription) { this.receivedSubscription.unsubscribe() };
    if (!!this.givenSubscription) { this.givenSubscription.unsubscribe() };
    if (!!this.scrollSubscription) { this.scrollSubscription.unsubscribe() };
    if (!!this.statusSubscription) { this.statusSubscription.unsubscribe() };
    if (!!this.updateSubscription) { this.updateSubscription.unsubscribe() };
  }

  private updateStatuses(): void {
    this.feedbacksReceived.forEach((item, index) => {
      if (_includes(this.seenThreads, item.threadId)) {
        this.feedbacksReceived[index].isRead = true;
      }
    });
    this.feedbacksGiven.forEach((item, index) => {
      if (_includes(this.seenThreads, item.threadId)) {
        this.feedbacksGiven[index].isRead = true;
      }
    });
  }

  private getReceivedFeedback(index?: number): void {
    this.isFetching = true;
    this.receivedSubscription = this.service.getFeedbacks('feedback/received', index, this.resultsPerPage)
      .subscribe((data: any) => {
        this.isFetching = false;
        const _data =  data.json().feedbacks;
        if(_data && _data.length > 0) {
          const mergedResults = uniqWith([..._data, ...this.feedbacksReceived], isEqual);
          this.feedbacksReceived = this.service.sortByDate(mergedResults);
        }
      },(error: any) => {
        this.isFetching = false;
        console.error(error);
      });
  }
  private getGivenFeedback(index?: number): void  {
    this.isFetching = true;
    this.givenSubscription = this.service.getFeedbacks('feedback/given', index, this.resultsPerPage)
      .subscribe((data: any) => {
        this.isFetching = false;
        const _data =  data.json().feedbacks;
        if(_data && _data.length > 0) {
          const mergedResults = uniqWith([..._data, ...this.feedbacksGiven], isEqual);
          this.feedbacksGiven = this.service.sortByDate(mergedResults);
        }
      },(error: any) => {
        this.isFetching = false;
        console.error(error);
      });
  }

  getFeedback() {
    this.pubSub.$pub('EVT_OVERLAY_SEARCH', 'request');
  }
  giveFeedback() {
    this.pubSub.$pub('EVT_OVERLAY_SEARCH', 'give');
  }

}
