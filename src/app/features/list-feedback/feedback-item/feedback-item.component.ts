import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Feedback } from '../../../interfaces/feedback.interface';

import { CustomEmotion } from 'ng2-emojify';
import { emojis } from '../../../app.config';
import { PubSubService } from 'angular2-pubsub';

@Component({
  selector: 'app-feedback-item',
  templateUrl: './feedback-item.component.html',
  styleUrls: ['./feedback-item.component.scss']
})
export class FeedbackItemComponent implements OnInit {

  @Input() feedback: Feedback;
  @Input() style: string;


  constructor(private router: Router, private twemojis: CustomEmotion, private pubSub: PubSubService) {
    this.twemojis.AddCustomEmotions(emojis);
  }

  ngOnInit() {

  }

  getFeedbackThread($event, threadId: number): void {
    if(this.style === 'TAB') {
      this.router.navigate(['thread/view', threadId]);
    } else {
      this.pubSub.$pub('EVT_OVERLAY_THREAD', threadId);
    }
  }

}
