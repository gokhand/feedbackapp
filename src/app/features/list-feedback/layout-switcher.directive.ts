import { Directive, OnInit, OnDestroy } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { Router } from '@angular/router';
import { config } from '../../app.config';
import { Subject } from 'rxjs/Subject';

@Directive({
  selector: '[appLayoutSwitcher]'
})
export class LayoutSwitcherDirective implements OnInit, OnDestroy {

  private resizeSubscription: any;
  private rerouteStartSubscription: any;
  private rerouteEndSubscription: any;

  private mergeSubject = new Subject();

  private breakpoint = config.resize.screenMd;

  constructor(private pubSub: PubSubService, private router: Router) {
    this.resizeSubscription = this.pubSub.$sub('EVT_WINDOW_RESIZED').subscribe((state) => {
      this.mergeSubject.next('is_resized');
    });

    this.rerouteStartSubscription = this.pubSub.$sub('EVT_NAVIGATION_START').subscribe((state) => {
      this.mergeSubject.next('is_rerouting');
    });

    this.rerouteEndSubscription = this.pubSub.$sub('EVT_NAVIGATION_END').subscribe((state) => {
      this.mergeSubject.next('is_not_rerouting');
    });

    this.mergeSubject.subscribe((state) => {
      if (state === 'is_resized' && state !== 'is_rerouting') {
        this.redirect();
      }
    });
  }

  ngOnInit() {
    this.initialCheck();
  }

  ngOnDestroy() {
    this.resizeSubscription.unsubscribe();
    this.rerouteStartSubscription.unsubscribe();
    this.rerouteEndSubscription.unsubscribe();
    this.mergeSubject.unsubscribe();
  }

  private windowSize(): number {
    return window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
  }

  private redirect(): void {
    const w = this.windowSize();
    w > this.breakpoint ?
      this.router.navigate(['feedback/split']) : this.router.navigate(['/']);
  }

  private initialCheck(): void {
    const w = this.windowSize();
    w > this.breakpoint ? this.router.navigate(['feedback/split']) : () => { };
  }

}
