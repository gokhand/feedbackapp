import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SpinnerModule } from '../../components/spinner/spinner.module';

import { Ng2EmojifyModule } from 'ng2-emojify';

import { LoginGuard } from '../../guards/login.guard';
import { AvatarModule } from '../../components/avatar/avatar.module';

import { GivenComponent } from './given/given.component';
import { ReceivedComponent } from './received/received.component';
import { FeedbackItemComponent } from './feedback-item/feedback-item.component';
import { SplitComponent } from './split/split.component';
import { ListFeedbackService } from './list-feedback.service';
import { DirectivesModule } from '../../directives/directives.module';
import { LayoutSwitcherDirective } from './layout-switcher.directive';

const routes: Routes = [
  { path: 'split', component: SplitComponent, canActivate: [LoginGuard], data: { name: 'splitFeedback'} },
  { path: 'given', component: GivenComponent, canActivate: [LoginGuard], data: { name: 'givenFeedback' } },
  { path: 'received', component: ReceivedComponent, canActivate: [LoginGuard], data: { name: 'receivedFeedback' } },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AvatarModule,
    Ng2EmojifyModule,
    SpinnerModule,
    DirectivesModule
  ],
  declarations: [
    GivenComponent,
    ReceivedComponent,
    FeedbackItemComponent,
    SplitComponent,
    LayoutSwitcherDirective
  ],
  exports: [LayoutSwitcherDirective],
  providers: [
    ListFeedbackService,
    LoginGuard
  ]
})
export class ListFeedbackModule {

}
