import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListFeedbackService } from '../list-feedback.service';
import { PubSubService } from 'angular2-pubsub';
import { config } from '../../../app.config';
import { uniqWith, isEqual } from 'lodash';

@Component({
  selector: 'app-given',
  templateUrl: './given.component.html',
  styleUrls: ['./given.component.scss']
})
export class GivenComponent implements OnInit, OnDestroy {

  public feedbacks: Array<any> = [];
  private subscription: any;
  private scrollSubscription: any;
  private updateSubscription: any;
  private scrollIndex = 0;
  public isFetching: boolean;
  private resultsPerPage: number = config.feedbacks.resultsPerPage;

  constructor(private service: ListFeedbackService, private pubSub: PubSubService) {
    this.scrollSubscription = this.pubSub.$sub('EVT_ELEM_SCROLL_END').subscribe((state) => {
      this.scrollIndex++;
      this.getFeedbacks(this.scrollIndex);
    });

    this.updateSubscription = this.pubSub.$sub('EVT_NEW_FEEDBACK').subscribe((type: string) => {
      this.getFeedbacks(this.scrollIndex);
    });
  }

  ngOnInit() {
    this.scrollIndex = 0;
    this.getFeedbacks(0);
  }
  ngOnDestroy() {
    if (!!this.subscription) { this.subscription.unsubscribe() };
    if (!!this.scrollSubscription) { this.scrollSubscription.unsubscribe() };
    if (!!this.updateSubscription) { this.updateSubscription.unsubscribe() };
    this.scrollIndex = 0;
    this.isFetching = false;
  }

  private getFeedbacks(index) {
    this.isFetching = true;
    this.subscription = this.service.getFeedbacks('feedback/given', index, this.resultsPerPage)
      .subscribe((_data: any) => {
        this.isFetching = false;
        const data = _data.json();
        if(data.feedbacks && data.feedbacks.length > 0) {
          const mergedResults = uniqWith([...data.feedbacks, ...this.feedbacks], isEqual);
          this.feedbacks = this.service.sortByDate(mergedResults);
        }
        this.pubSub.$pub('EVT_TAB_UNREAD_COUNT_UPDATE', data.aggregate);

      },(error: any) => {
        this.isFetching = false;
        console.error(error);
      });
  }

}
