import { Component, OnDestroy, OnInit } from '@angular/core';
import { PersonService } from '../../../services/person.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Recipient } from '../../../interfaces/recipient.interface';
import { ThreadService } from '../thread.service';
import { PubSubService } from 'angular2-pubsub';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit, OnDestroy {

  public recipient: Recipient;
  public isRecipientResolved: boolean;
  private feedbackType: string;

  constructor(
    private personService: PersonService,
    private router: Router,
    private pubSub: PubSubService,
    private threadService: ThreadService,
    private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.feedbackType = this.route.snapshot.params['type'] || 'get';
    this.recipient = this.personService.getPerson();
    if (this.recipient === undefined) {
      this.isRecipientResolved = false;
      history.back();
    } else {
      this.isRecipientResolved = true;
    }
  }

  ngOnDestroy() {

  }

  public onNewMessage(message: string): void {
    if(message) {
      const dto = {
        employee: this.recipient,
        message: message
      };
      this.threadService.create(`feedback/${this.feedbackType}`, dto).subscribe((threadId) => {
        this.router.navigate(['thread/view', threadId]);

        this.pubSub.$pub('EVT_TOASTR', {
          type: 'SUCCESS',
          message: `${this.feedbackType} feedback successful`
        });

        this.pubSub.$pub('EVT_NEW_FEEDBACK', 'give');

        this.report();

      }, (error) => {
        this.pubSub.$pub('EVT_TOASTR', {
          type: 'ERROR',
          message: `We couldn't process your request. Please try again later.`
        });
      });
    }
  }

  private report(): void {
    this.pubSub.$pub('EVT_ANALYTICS', {
      'category': 'FEEDBACK',
      'action': this.feedbackType,
      'label': '',
      'value': 0
    });
  }

  public onComposerResize($event): void {

  }

  public changeRecipient(): void {
    this.router.navigate([`search/${this.feedbackType}`]);
  }

}
