import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ThreadService {

  constructor(private http: Http) { }

  public create(resource, dto): Observable<object> {
    return this.http.post(`${resource}`, dto)
      .map((res: Response) => res.json());
  }

}
