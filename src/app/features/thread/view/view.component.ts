import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-thread-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit, OnDestroy {

  public threadId: number;

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.threadId = this.route.snapshot.params['threadId'] || 0;
  }

  ngOnDestroy() {

  }

}
