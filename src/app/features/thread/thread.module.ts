import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PersonModule } from '../../components/person/person.module';
import { MessagesModule } from '../../components/messages/messages.module';
import { ComposerModule } from '../../components/composer/composer.module';
import { LoginGuard } from '../../guards/login.guard';
import { NewComponent } from './new/new.component';
import { ViewComponent } from './view/view.component';
import { ThreadService} from './thread.service';

const routes: Routes = [
  {
    path: 'view/:threadId',
    component: ViewComponent,
    canActivate: [LoginGuard],
    data: { name: 'threadView'}
  },
  {
    path: 'new/:type',
    component: NewComponent,
    canActivate: [LoginGuard],
    data: { name: 'threadNew'}
  }
];

@NgModule({
  imports: [
    CommonModule,
    PersonModule,
    RouterModule.forChild(routes),
    MessagesModule,
    ComposerModule
  ],
  declarations: [
    NewComponent,
    ViewComponent
  ],
  providers: [
    LoginGuard,
    ThreadService
  ],
  exports: []
})
export class ThreadModule { }
