const emojiBase = 'https://twemoji.maxcdn.com/svg/';

export const config = {
  'resize': {
    'debounce': 100,
    'screenMd': 996
  },
  'scroll': {
    'debounce': 300,
    'offset': 50
  },
  'toastrDefaults': {
    'timeout': 5000,
    'type': 'SUCCESS',
    'message': ''
  },
  'feedbacks': {
    'resultsPerPage': 10
  },
  'peoplefinder': {
    'maxResults': 100
  }
};

export const emojis = [
  {
    'emojiId': 'award',
    'emojiUrl': `${emojiBase}1f3c6.svg`,
    'title': 'Award'
  },
  {
    'emojiId': 'listen',
    'emojiUrl': `${emojiBase}1f442.svg`,
    'title': 'Listen'
  },
  {
    'emojiId': 'aok',
    'emojiUrl': `${emojiBase}1f44c.svg`,
    'title': 'AOK'
  },
  {
    'emojiId': 'thumbup',
    'emojiUrl': `${emojiBase}1f44d.svg`,
    'title': 'Thumb Up'
  },
  {
    'emojiId': 'thumbdown',
    'emojiUrl': `${emojiBase}1f44e.svg`,
    'title': 'Thumb Down'
  },
  {
    'emojiId': 'clap',
    'emojiUrl': `${emojiBase}1f44f.svg`,
    'title': 'Clap'
  },
  {
    'emojiId': 'bulb',
    'emojiUrl': `${emojiBase}1f4a1.svg`,
    'title': 'Bulb'
  },
  {
    'emojiId': 'star',
    'emojiUrl': `${emojiBase}2b50.svg`,
    'title': 'Star'
  },
  {
    'emojiId': 'idea',
    'emojiUrl': `${emojiBase}1f4ad.svg`,
    'title': 'Idea'
  },
  {
    'emojiId': 'usd',
    'emojiUrl': `${emojiBase}1f4b2.svg`,
    'title': 'Panic'
  },
  {
    'emojiId': 'gains',
    'emojiUrl': `${emojiBase}1f4c8.svg`,
    'title': 'Gains'
  },
  {
    'emojiId': 'losses',
    'emojiUrl': `${emojiBase}1f4c9.svg`,
    'title': 'Losses'
  },
  {
    'emojiId': 'security',
    'emojiUrl': `${emojiBase}1f510.svg`,
    'title': 'Security'
  },
  {
    'emojiId': 'clock',
    'emojiUrl': `${emojiBase}1f565.svg`,
    'title': 'Clock'
  },
  {
    'emojiId': 'smile',
    'emojiUrl': `${emojiBase}1f600.svg`,
    'title': 'Smile'
  },
  {
    'emojiId': 'meh',
    'emojiUrl': `${emojiBase}1f610.svg`,
    'title': 'Meh'
  },
  {
    'emojiId': 'cool',
    'emojiUrl': `${emojiBase}1f60e.svg`,
    'title': 'Cool'
  },
  {
    'emojiId': 'hey',
    'emojiUrl': `${emojiBase}1f64b.svg`,
    'title': 'Hey'
  },
  {
    'emojiId': 'hourglass',
    'emojiUrl': `${emojiBase}231b.svg`,
    'title': 'Hourglass'
  },
  {
    'emojiId': 'concern',
    'emojiUrl': `${emojiBase}1f615.svg`,
    'title': 'Concerned'
  },
  {
    'emojiId': 'win',
    'emojiUrl': `${emojiBase}1f4ab.svg`,
    'title': 'Win'
  },
  {
    'emojiId': 'target',
    'emojiUrl': `${emojiBase}1f3af.svg`,
    'title': 'Target'
  }
];
