import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchOverlayComponent } from './search-overlay.component';
import { LookupModule } from '../lookup/lookup.module';
import { ThreadService } from '../../features/thread/thread.service';

@NgModule({
  imports: [
    CommonModule,
    LookupModule
  ],
  declarations: [SearchOverlayComponent],
  exports: [SearchOverlayComponent],
  providers: [ThreadService]
})
export class SearchOverlayModule { }
