import {Component, OnDestroy, OnInit} from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { PersonService } from '../../services/person.service';
import { Person } from '../../interfaces/person.interface';

@Component({
  selector: 'app-search-overlay',
  templateUrl: './search-overlay.component.html',
  styleUrls: ['./search-overlay.component.scss']
})
export class SearchOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public feedbackType: string;
  public isVisible: boolean;

  constructor(
    private pubSub: PubSubService,
    private personService: PersonService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_OVERLAY_SEARCH').subscribe((_type) => {
      this.feedbackType = _type;
      this.isVisible = true;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public personPicked(person: Person): void {
    this.personService.setPerson(person);
    this.pubSub.$pub('EVT_OVERLAY_COMPOSE', this.feedbackType);
    this.closeModal();
  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
