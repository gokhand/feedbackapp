import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { MessagesService } from './messages.service';
import { Subject } from 'rxjs/Subject';
import { CustomEmotion } from 'ng2-emojify';
import { emojis } from '../../app.config';
import { Correspondent } from '../../interfaces/correspondent.interface';
import {PubSubService} from 'angular2-pubsub';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit, OnDestroy, OnChanges {

  public messageCollection: Array<any> = [];
  public correspondent: Correspondent;
  public isFetching: boolean;
  private messageSubscription: any;
  private getSubscription: any;
  private messageSubject = new Subject();
  private threadId: string;
  private timer: any;
  private scrollIndex = 0;

  @Input() id: string;
  @Input() isNew: boolean;
  @Output() onFromResolved = new EventEmitter<string>();

  constructor(private service: MessagesService, private twemojis: CustomEmotion, private pubSub: PubSubService) {
    this.twemojis.AddCustomEmotions(emojis);
    this.messageSubscription = this.messageSubject.subscribe((data: any[]) => {
      this.messageCollection = data;
    });
  }

  ngOnInit() {
    this.threadId = this.id;
  }

  ngOnDestroy() {
    if (!!this.messageSubscription) { this.messageSubscription.unsubscribe() };
    if (!!this.getSubscription) { this.getSubscription.unsubscribe() };
    if (this.timer) { clearTimeout(this.timer); }
  }

  ngOnChanges(changes: any) {
    const id = changes.id.currentValue;
    this.getMessages(id, this.scrollIndex);
    this.threadId = id;
  }

  public onComposerResize(height): void {
    const messages = document.getElementById('message-list');
    messages.style.height = `${window.innerHeight - 160 - height}px`;
    this.scrollToBottom();
  }

  public onNewMessage(message?: String): void {
    let subscription: any;
    subscription = this.service.reply('feedback/reply', this.threadId, {
      id: 1,
      threadId: this.threadId,
      employee: this.correspondent,
      message
    }).subscribe((data: any) => {
      this.updateMessages(message);
      this.pubSub.$pub('EVT_NEW_FEEDBACK');
      subscription.unsubscribe();
    });
  }

  private threadRead(id?: string): void {
    this.pubSub.$pub('EVT_THREAD_SEEN', id);
  }

  private scrollToBottom(): void {
    const messages = document.getElementById('message-list');
    messages.scrollTop = messages.scrollHeight - messages.clientHeight;
  }

  private updateMessages(message?: String): void {
    const messages = this.messageCollection;
    const newMessage = {
      dateCreated: new Date(),
      isMine: true,
      message
    };
    messages.push(newMessage);
    this.messageSubject.next(messages);
    this.scrollToBottom();
  }

  private reportCount(count?: number): void {
    this.pubSub.$pub('EVT_ANALYTICS', {
      'category': 'THREAD',
      'action': 'view_messages',
      'label': 'message count',
      'value': count
    });
  }

  private getMessages(id?: string, index?: number): void {
    this.isFetching = true;
    this.getSubscription = this.service.getMessages('feedback/thread', id, index, 1000).subscribe((data: any) => {
      this.isFetching = false;
      this.messageSubject.next(data.feedbacks);
      this.reportCount(data.feedbacks.length);
      this.correspondent = data.employee;
      this.onFromResolved.emit(data.employee);
      this.threadRead(id);
      this.timer = setTimeout(() => {
        this.scrollToBottom();
      }, 500);
    },(error: any) => {
      this.isFetching = false;
      console.error(error);
    });
  }

}
