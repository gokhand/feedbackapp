import { Component, OnInit, Input } from '@angular/core';
import { MessageItem } from '../../../interfaces/message-item.interface';
import { Correspondent } from '../../../interfaces/correspondent.interface';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit {

  @Input() messageItem: MessageItem;
  @Input() correspondent: Correspondent;

  constructor() {

  }

  ngOnInit() {

  }

}
