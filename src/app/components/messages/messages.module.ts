import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvatarModule } from '../avatar/avatar.module';
import { EmojiPickerModule } from '../emoji-picker/emoji-picker.module';
import { ComposerModule } from '../composer/composer.module';
import { Ng2EmojifyModule } from 'ng2-emojify';
import { MessageItemComponent } from './message-item/message-item.component';
import { MessagesComponent } from './messages.component';
import { MessagesService } from './messages.service';
import { SpinnerModule } from '../spinner/spinner.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    Ng2EmojifyModule,
    AvatarModule,
    EmojiPickerModule,
    ComposerModule,
    SpinnerModule,
    DirectivesModule
  ],
  declarations: [MessagesComponent, MessageItemComponent],
  exports: [MessagesComponent, MessageItemComponent],
  providers: [MessagesService]
})
export class MessagesModule { }
