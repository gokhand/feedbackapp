import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MessagesService {

  constructor(private http: Http) { }

  getMessages(resource, threadId, page, pageSize) {
    return this.http.get(`${resource}/${threadId}?page=${page}&pageSize=${pageSize}`)
      .map((res: Response) => res.json());
  }

  reply(resource, threadId, dto) {
    return this.http.put(`${resource}?threadId=${threadId}`, dto)
      .map((res: Response) => res.json());
  }

}
