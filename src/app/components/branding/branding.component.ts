import { Component, OnInit, OnDestroy } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-branding',
  templateUrl: './branding.component.html',
  styleUrls: ['./branding.component.scss']
})
export class BrandingComponent implements OnInit, OnDestroy {

  public isActive: boolean;
  public fullName: string;
  private subscription: any;

  constructor(private pubSub: PubSubService, private sessionService: SessionService) { }

  ngOnInit() {

    this.sessionService.getUser().subscribe((user: object) => {
      let name = '';
      if (user && user.hasOwnProperty('fullName')) {
        name = user['fullName'];
      } else {
        const savedUser = JSON.parse(localStorage.getItem('user'));
        name = savedUser ? savedUser['fullName'] : 'user';
      }
      name = name.split(',')[1];
      this.fullName = name;
    });
    this.subscription = this.pubSub.$sub('EVT_TOGGLE_OPENER').subscribe((isOpen: boolean) => {
      this.isActive = isOpen;
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  public toggleDropdown($event): void {
    this.isActive = !this.isActive;
    const target = document.getElementById('app_toggle');
    this.pubSub.$pub('EVT_TOGGLE_ACCOUNT_DROPDOWN', {
      posX: target.getBoundingClientRect().left + target.offsetWidth || 0
    });
  }

}
