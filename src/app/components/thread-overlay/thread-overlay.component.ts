import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { From } from '../../interfaces/from.interface';

@Component({
  selector: 'app-thread-overlay',
  templateUrl: './thread-overlay.component.html',
  styleUrls: ['./thread-overlay.component.scss']
})
export class ThreadOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public isVisible: boolean;
  public threadId: number;
  public fromName: string;

  constructor(private pubSub: PubSubService, private cdr: ChangeDetectorRef) {
    this.subscription = this.pubSub.$sub('EVT_OVERLAY_THREAD').subscribe((threadId: number) => {
      this.threadId = threadId;
      this.isVisible = true;
      this.cdr.detectChanges();
    });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public fromResolved(person?: From): void {
    this.fromName = person.fullName;
    this.cdr.detectChanges();
  }

  public closeModal(): void {
    this.isVisible = false;
    this.fromName = '';
    this.cdr.detectChanges();
  }

}
