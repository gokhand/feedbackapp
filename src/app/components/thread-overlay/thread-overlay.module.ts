import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreadOverlayComponent } from './thread-overlay.component';
import { MessagesModule } from '../../components/messages/messages.module';

@NgModule({
  imports: [
    CommonModule,
    MessagesModule
  ],
  declarations: [ThreadOverlayComponent],
  exports: [ThreadOverlayComponent]
})
export class ThreadOverlayModule { }
