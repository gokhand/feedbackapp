import { Component, OnInit, OnDestroy } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { SessionService } from '../../services/session.service';
import { Myuser } from '../../interfaces/myuser.interface';

@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.scss']
})
export class AccountMenuComponent implements OnInit, OnDestroy {

  public isVisible: boolean;
  public myuser: Myuser;
  private subscription: any;

  constructor(private pubSub: PubSubService, private sessionService: SessionService) {

  }

  ngOnInit() {
    const el = document.getElementById('account-menu');
    this.subscription = this.pubSub.$sub('EVT_TOGGLE_ACCOUNT_DROPDOWN').subscribe((config) => {
      el.style.left = `${config.posX - el.offsetWidth }px`;
      this.isVisible = !this.isVisible;
    });
    this.sessionService.getUser().subscribe((user: Myuser) => {
      if (user && user.hasOwnProperty('fullName')) {
        this.myuser = user;
      } else {
        const savedUser = JSON.parse(localStorage.getItem('user'));
        this.myuser = savedUser ? savedUser : {};
      }
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  public closeMenu(): void {
    this.isVisible = false;
    this.pubSub.$pub('EVT_TOGGLE_OPENER', false);
  }
  public logout(): void {
    this.sessionService.logout();
  }

  public showLegalModal(): void {
    this.pubSub.$pub('EVT_SHOW_LEGAL', true);
  }
  public showPrivacyModal(): void {
    this.pubSub.$pub('EVT_SHOW_PRIVACY', true);
  }

  public showAbout(): void {
    this.pubSub.$pub('EVT_SHOW_ABOUT');
  }

  public showTips(): void {
    this.pubSub.$pub('EVT_SHOW_TIPS');
  }


}
