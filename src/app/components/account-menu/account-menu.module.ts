import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountMenuComponent } from './account-menu.component';
import { AvatarModule } from '../avatar/avatar.module';

@NgModule({
  imports: [
    CommonModule,
    AvatarModule
  ],
  declarations: [AccountMenuComponent],
  exports: [AccountMenuComponent]
})
export class AccountMenuModule { }
