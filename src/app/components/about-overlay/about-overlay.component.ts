import {Component, OnDestroy, OnInit} from '@angular/core';
import {PubSubService} from 'angular2-pubsub';

@Component({
  selector: 'app-about-overlay',
  templateUrl: './about-overlay.component.html',
  styleUrls: ['./about-overlay.component.scss']
})
export class AboutOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public isVisible: boolean;

  constructor(private pubSub: PubSubService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_SHOW_ABOUT').subscribe(() => {
      this.isVisible = true;
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
