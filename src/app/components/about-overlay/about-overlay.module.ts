import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutOverlayComponent } from './about-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AboutOverlayComponent],
  exports: [AboutOverlayComponent]
})
export class AboutOverlayModule { }
