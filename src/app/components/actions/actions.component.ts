import { Component, OnInit, OnDestroy } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { config } from '../../app.config';
import { includes as _includes } from 'lodash';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit, OnDestroy {

  public showActions = true;
  private resizeSubscription: any;
  private rerouteSubscription: any;
  private breakpoint = config.resize.screenMd;

  constructor(private pubSub: PubSubService) { }

  ngOnInit() {
    this.resizeSubscription = this.pubSub.$sub('EVT_WINDOW_RESIZED').subscribe((state) => {
      this.toggle();
    });
    this.rerouteSubscription = this.pubSub.$sub('EVT_NAVIGATION_END').subscribe((state) => {
      this.toggle(state);
    });
  }

  ngOnDestroy() {
    this.resizeSubscription.unsubscribe();
    this.rerouteSubscription.unsubscribe();
  }

  isDrilldown(state?: object): boolean {
    const _state = document.location.toString();
    return _includes(_state, 'feedback/given') ||
           _includes(_state, 'feedback/received');
  }

  toggle(state?: object) {
    let w = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    if(w > this.breakpoint) {
      this.showActions = false;
    }
    else if( w < this.breakpoint && this.isDrilldown(state)){
      this.showActions = true;
    }
    else if( w < this.breakpoint && !this.isDrilldown(state)) {
      this.showActions = false;
    }

  }

}
