import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmojiPickerComponent } from './emoji-picker.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EmojiPickerComponent],
  exports: [EmojiPickerComponent]
})
export class EmojiPickerModule { }
