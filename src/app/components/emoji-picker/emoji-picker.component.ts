import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { emojis } from '../../app.config';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-emoji-picker',
  templateUrl: './emoji-picker.component.html',
  styleUrls: ['./emoji-picker.component.scss']
})
export class EmojiPickerComponent implements OnInit, OnDestroy {

  emojiCollection: Array<any> = [];
  private emojiSubscription: any;
  private emojiSubject = new Subject();
  private timer: any;
  public isVisible = false;
  public posY: String;

  @Output() onEmojiPicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.emojiSubscription = this.emojiSubject.subscribe((data: any[]) => {
      this.emojiCollection = data;
    });
    this.emojiSubscription.next(emojis);
  }
  ngOnDestroy() {
    this.emojiSubscription.unsubscribe();
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

  public keyboardBlur(): void {
    this.timer = setTimeout(() => {
      const button = document.getElementById('app_emoji_button');
      const picker = document.getElementById('emojipickerwrapper');
      const h = picker.clientHeight;
      this.posY = `${button.offsetTop - h - 48}px`;
      this.isVisible = !this.isVisible;
    }, 300);
  }

  public toggleVisibility(event): void {
    const picker = document.getElementById('emojipickerwrapper');
    const h = picker.clientHeight;
    this.posY = `${event.clientY - h - 48}px`;
    this.isVisible = !this.isVisible;
  };

  public pickEmoji(id): void {
    this.isVisible = false;
    this.onEmojiPicked.emit(id);
  }

}
