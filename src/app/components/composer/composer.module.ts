import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComposerComponent } from './composer.component';
import { EmojiPickerModule } from '../emoji-picker/emoji-picker.module';
import { Ng2EmojifyModule } from 'ng2-emojify';

@NgModule({
  imports: [
    CommonModule,
    EmojiPickerModule,
    Ng2EmojifyModule
  ],
  declarations: [ComposerComponent],
  exports: [ComposerComponent]
})
export class ComposerModule { }
