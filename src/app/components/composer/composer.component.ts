import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { CustomEmotion } from 'ng2-emojify';
import { EmojiPickerComponent } from '../emoji-picker/emoji-picker.component';
import { emojis } from '../../app.config';
import {PubSubService} from 'angular2-pubsub';

@Component({
  selector: 'app-composer',
  templateUrl: './composer.component.html',
  styleUrls: ['./composer.component.scss']
})
export class ComposerComponent implements OnInit, OnDestroy {

  public message = '';
  private composeSubscription: any;
  private changeSubscription: any;
  private heightSubscription: any;
  private messageSubject = new Subject();
  private changeSubject = new Subject();
  private heightSubject = new Subject();
  private initialHeight: number;

  @Output() onSubmit = new EventEmitter<string>();
  @Output() onResize = new EventEmitter<string>();

  @ViewChild(EmojiPickerComponent) emojiPicker: EmojiPickerComponent;

  constructor(private twemojis: CustomEmotion, private zone: NgZone, private pubSub: PubSubService) {
    this.twemojis.AddCustomEmotions(emojis);

    this.composeSubscription = this.messageSubject
        .distinctUntilChanged()
        .subscribe((value: string) => {
          this.message = value;
        });

    this.heightSubscription = this.heightSubject
        .debounceTime(500)
        .subscribe((value: string) => {
          this.onResize.emit(value);
        });
  }

  ngOnInit() {
    this.watchTextarea();
    this.initialHeight = document.getElementById('composer-textarea').clientHeight;

  }
  ngOnDestroy() {
    if(!!this.composeSubscription) { this.composeSubscription.unsubscribe() };
    if(!!this.changeSubscription) { this.changeSubscription.unsubscribe() };
    if(!!this.heightSubscription) { this.heightSubscription.unsubscribe() };
  }

  public onEmojiPicked(id: string): void {
    const newValue = `${this.message} :${id}:`;
    this.messageSubject.next(newValue);
    document.getElementById('composer-textarea').focus();
  }

  public toggleEmojiPicker(event): void {
    this.emojiPicker.toggleVisibility(event);
  }

  private resize(textarea): void {
    const viewer = document.getElementById('composer-viewer');
    const height = textarea.scrollHeight;
    this.messageSubject.next(textarea['value']);
    if(textarea.clientHeight < textarea.scrollHeight) {
      textarea.style.height = viewer.style.height = `${height + 5}px`;
    } else {
      textarea.style.height = viewer.style.height = `${height - 5}px`;
    }
    this.heightSubject.next(height);
  }

  public onBlur(): void {
    this.emojiPicker.keyboardBlur();
  }

  public watchTextarea(): void {
    const textarea = document.getElementById('composer-textarea');
    const viewer = document.getElementById('composer-viewer');
    textarea.style.width = `${viewer.scrollWidth}px`;
    this.zone.runOutsideAngular(() => {
      Observable.fromEvent(textarea, 'keyup')
        .subscribe((e: Event) => {
            this.zone.run(() => {
              this.changeSubject.next(e);
            });
          }
        );
    });
    this.changeSubscription = this.changeSubject.subscribe((e: Event) => {
      this.resize(textarea);
    });
  }

  public lineBreak($event): void {
    this.messageSubject.next(event.target['value'] + '<br/>');
  }

  private strip(msg: string): string {
    const strippedMsg = msg.replace(/<script.*?>.*?<\/script>/igm, '');
    return strippedMsg;
  }

  public submit(): void {
    const _before = this.message;
    const copy = this.strip(_before);
    if(copy.length > 0) {
      this.onSubmit.emit(copy);
      const textarea = document.getElementById('composer-textarea');
      this.messageSubject.next('');
      textarea['value'] = '';
      textarea.style.height = `${this.initialHeight}px`;
      this.resize(textarea);
    } else {
      this.pubSub.$pub('EVT_TOASTR', {
        type: 'ERROR',
        message: `You can't send an empty message`,
        timeout: 500
      });
    }
  }

}
