import {Component, OnDestroy, OnInit} from '@angular/core';
import {PubSubService} from 'angular2-pubsub';

@Component({
  selector: 'app-tips-overlay',
  templateUrl: './tips-overlay.component.html',
  styleUrls: ['./tips-overlay.component.scss']
})
export class TipsOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public isVisible: boolean;

  constructor(private pubSub: PubSubService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_SHOW_TIPS').subscribe(() => {
      this.isVisible = true;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
