import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TipsOverlayComponent } from './tips-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TipsOverlayComponent],
  exports: [TipsOverlayComponent]
})
export class TipsOverlayModule { }
