import {Component, OnDestroy, OnInit} from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { AppService } from '../../app.service';
import { SessionService } from '../../services/session.service';
import { Myuser } from '../../interfaces/myuser.interface';

@Component({
  selector: 'app-legal-overlay',
  templateUrl: './legal-overlay.component.html',
  styleUrls: ['./legal-overlay.component.scss']
})
export class LegalOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public isVisible: boolean;
  public isCancellable: boolean;

  constructor(private pubSub: PubSubService, private appService: AppService, private sessionService: SessionService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_SHOW_LEGAL').subscribe((_isCancellable: boolean) => {
      this.isCancellable = _isCancellable;
      this.isVisible = true;
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public agree(): void {
    this.sessionService.getUser().subscribe((_user: Myuser) => {
      const dto = {
        hasAgreedLegal: true
      };
      this.appService.update(`employee/agreedlegal?id=${_user.id}`, dto ).subscribe(() => {
        this.pubSub.$pub('EVT_AGREED_LEGAL', true);
        this.closeModal();
      });
    });
  }

  public cancel(): void {
    this.isCancellable ? this.closeModal() : this.sessionService.logout();
  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
