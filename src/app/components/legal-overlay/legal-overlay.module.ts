import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LegalOverlayComponent } from './legal-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LegalOverlayComponent],
  exports: [LegalOverlayComponent]
})
export class LegalOverlayModule { }
