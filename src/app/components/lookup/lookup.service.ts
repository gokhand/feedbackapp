import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { config } from '../../app.config';

@Injectable()
export class LookupService {

  constructor( private http: Http ) {

  }
  query(keyword?: string): Observable<object> {
    return this.http.get(`peoplefinder/search/${keyword}?max=${config.peoplefinder.maxResults}`)
      .map((res: Response) => {
        const _response = res.json();
        return _response['data'];
      });
  }
}
