import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { LookupService } from './lookup.service';
import { Subject } from 'rxjs/Subject';
import { Person } from '../../interfaces/person.interface';
import {PubSubService} from 'angular2-pubsub';


@Component({
  selector: 'app-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss']
})
export class LookupComponent implements OnInit, OnDestroy {

  public suggestions: Array<any> = [];
  public noResults: boolean;
  public isFetching: boolean;
  private subscription: any;
  private keywordSubject = new Subject();

  @Output() onPersonPicked = new EventEmitter<any>();

  constructor(private service: LookupService, private pubSub: PubSubService) { }

  ngOnInit() {
    this.subscription = this.keywordSubject
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe((value: string) => {
        this.service.query(value).subscribe((data: any[]) => {
          this.isFetching = false;
          this.suggestions = data;
          sessionStorage.setItem('lastquery', JSON.stringify(data));
          this.noResults = data.length === 0;
        }, (error: any) => {
          console.log(error, error['_body']);
          this.isFetching = false;
        });
    });

    const lastquery = sessionStorage.getItem('lastquery');

    if(lastquery){
      this.suggestions = JSON.parse(lastquery);
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public onClick(person?: Person): void {
    this.onPersonPicked.emit(person);
  }

  public query($event?): void {
    this.isFetching = true;
    this.keywordSubject.next($event.target.value || '');
  }

}
