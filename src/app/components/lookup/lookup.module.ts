import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LookupComponent } from './lookup.component';
import { LookupService } from './lookup.service';
import { AvatarModule } from '../avatar/avatar.module';
import { PersonModule } from '../person/person.module';
import { SpinnerModule } from '../spinner/spinner.module';

@NgModule({
  imports: [
    CommonModule,
    AvatarModule,
    PersonModule,
    SpinnerModule
  ],
  declarations: [LookupComponent],
  providers: [LookupService],
  exports: [LookupComponent]
})
export class LookupModule { }
