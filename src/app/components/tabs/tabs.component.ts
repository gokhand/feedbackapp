import { Component, OnInit, OnDestroy } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { config } from '../../app.config';
import { Notifications } from '../../interfaces/notifications';
import { Router } from '@angular/router';
import { includes as _includes } from 'lodash';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, OnDestroy {

  public showTabs: boolean;
  public showBack: boolean;
  public notifications: Notifications;
  private resizeSubscription: any;
  private rerouteSubscription: any;
  private routingSubscription: any;
  private unreadSubscription: any;
  private breakpoint = config.resize.screenMd;
  private activatedFromRoute: string;

  constructor(private pubSub: PubSubService, private router: Router ) { }

  ngOnInit() {
    this.showTabs = true;
    this.resizeSubscription = this.pubSub.$sub('EVT_WINDOW_RESIZED').subscribe((state) => {
      this.toggle();
    });
    this.rerouteSubscription = this.pubSub.$sub('EVT_NAVIGATION_END').subscribe((state) => {
      this.toggle(state);
    });
    this.unreadSubscription = this.pubSub.$sub('EVT_TAB_UNREAD_COUNT_UPDATE').subscribe((_notifications: Notifications) => {
      this.notifications = _notifications;
    });

    this.routingSubscription = this.pubSub.$sub('EVT_NAVIGATION_VALID').subscribe((event: any) => {
      const url = event.state.url;
      if (_includes(url, 'feedback')) {
        this.activatedFromRoute = url;
      }
    });
  }

  ngOnDestroy() {
    this.resizeSubscription.unsubscribe();
    this.rerouteSubscription.unsubscribe();
    this.unreadSubscription.unsubscribe();
    this.routingSubscription.unsubscribe();
  }

  private isDrilldown(state?: object): boolean {
    /* NOTE: CHANGE THIS LATER ON WITH PROPER CUSTOM DATA ON ROUTE TO CHECK IF IT'S DRILLDOWN */
    const _state = document.location.toString();
    return _includes(_state, 'thread')
      || _includes(_state, 'search');

  }

  public goBack(): void {
    this.activatedFromRoute ?
        this.router.navigate([this.activatedFromRoute]) :
        this.router.navigate(['feedback/received']);
  }

  private toggle(state?: object): void {
    const w = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    if(w > this.breakpoint) {
      this.showTabs = false;
      this.showBack = false;
    } else if ( w < this.breakpoint && this.isDrilldown(state)) {
      this.showTabs = false;
      this.showBack = true;
    } else if ( w < this.breakpoint && !this.isDrilldown(state)) {
      this.showTabs = true;
      this.showBack = false;
    }

  }

}
