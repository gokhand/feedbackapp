import {Component, OnDestroy, OnInit} from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { AppService } from '../../app.service';
import { Myuser } from '../../interfaces/myuser.interface';
import {SessionService} from '../../services/session.service';

@Component({
  selector: 'app-privacy-overlay',
  templateUrl: './privacy-overlay.component.html',
  styleUrls: ['./privacy-overlay.component.scss']
})
export class PrivacyOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  private user: any;
  public isVisible: boolean;
  public isCancellable: boolean;

  constructor(private pubSub: PubSubService, private appService: AppService, private sessionService: SessionService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_SHOW_PRIVACY').subscribe((_isCancellable: boolean) => {
      this.isCancellable = _isCancellable;
      this.isVisible = true;
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public agree(): void {
    this.sessionService.getUser().subscribe((_user: Myuser) => {
      const dto = {
        hasAgreedPrivacy: true
      };
      this.appService.update(`employee/agreedprivacy?id=${_user.id}`, dto ).subscribe(() => {
        this.pubSub.$pub('EVT_AGREED_PRIVACY', true);
        this.closeModal();
      });
    });
  }

  public cancel(): void {
    this.isCancellable ? this.closeModal() : this.sessionService.logout();
  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
