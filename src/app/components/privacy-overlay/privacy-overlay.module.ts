import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyOverlayComponent } from './privacy-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PrivacyOverlayComponent],
  exports: [PrivacyOverlayComponent]
})
export class PrivacyOverlayModule { }
