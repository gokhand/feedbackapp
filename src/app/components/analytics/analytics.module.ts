import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsComponent } from './analytics.component';
import { AnalyticsService } from './analytics.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AnalyticsComponent],
  exports: [AnalyticsComponent],
  providers: [AnalyticsService]
})
export class AnalyticsModule { }
