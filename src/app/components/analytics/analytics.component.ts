import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './analytics.service';
import { PubSubService } from 'angular2-pubsub';

@Component({
  selector: 'app-analytics',
  template: '<div></div>'
})
export class AnalyticsComponent implements OnInit {

  private subscription: any;

  constructor(private analyticsService: AnalyticsService, private pubSub: PubSubService ) { }

  ngOnInit() {
    this.analyticsService.init();
    this.subscription = this.pubSub.$sub('EVT_ANALYTICS').subscribe((action: any) => {
      this.analyticsService.report(action);
    });
  }

}
