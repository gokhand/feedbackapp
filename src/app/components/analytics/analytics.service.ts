import { Injectable } from '@angular/core';
import { Analytics } from '../../interfaces/analytics';
import { environment } from '../../../environments/environment';

@Injectable()
export class AnalyticsService {

  constructor() { }

  public init(): void {
    window['ga']('create', environment.googleAnalyticsId, 'auto');
  }

  public report(event: Analytics): void {
    try {
      window['ga']('send', 'event', event.category, event.action, event.label, event.value);
    } catch (e) {
      console.log(e);
    }
  }

}
