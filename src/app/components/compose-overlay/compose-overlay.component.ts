import { Component, OnDestroy, OnInit } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { PersonService } from '../../services/person.service';
import { Recipient } from '../../interfaces/recipient.interface';
import { ThreadService } from '../../features/thread/thread.service';

@Component({
  selector: 'app-compose-overlay',
  templateUrl: './compose-overlay.component.html',
  styleUrls: ['./compose-overlay.component.scss']
})
export class ComposeOverlayComponent implements OnInit, OnDestroy {

  private subscription: any;
  public feedbackType: string;
  public recipient: Recipient;
  public isRecipientResolved: boolean;
  public isVisible: boolean;

  constructor(
    private pubSub: PubSubService,
    private personService: PersonService,
    private threadService: ThreadService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_OVERLAY_COMPOSE').subscribe((_type) => {
      this.getRecipient();
      this.feedbackType = _type;
      this.isVisible = true;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getRecipient(): void {
    this.recipient = this.personService.getPerson();
    if (this.recipient === undefined) {
      this.isRecipientResolved = false;
    } else {
      this.isRecipientResolved = true;
    }
  }

  public onNewMessage(message: string): void {
    if(message) {
      const dto = {
        employee: this.recipient,
        message: message
      };
      this.threadService.create(`feedback/${this.feedbackType}`, dto).subscribe((threadId) => {
        this.pubSub.$pub('EVT_OVERLAY_THREAD', threadId);
        this.report();
        this.pubSub.$pub('EVT_TOASTR', {
          type: 'SUCCESS',
          message: `${this.feedbackType} feedback successful`
        });

        this.pubSub.$pub('EVT_NEW_FEEDBACK', 'give');

        this.closeModal();
      });
    }
  }

  private report(): void {
    this.pubSub.$pub('EVT_ANALYTICS', {
      'category': 'FEEDBACK',
      'action': this.feedbackType,
      'label': '',
      'value': 0
    });
  }

  public changeRecipient(): void {
    this.pubSub.$pub('EVT_OVERLAY_SEARCH', this.feedbackType);
    this.closeModal();
  }

  public onComposerResize($event): void {

  }

  public closeModal(): void {
    this.isVisible = false;
  }

}
