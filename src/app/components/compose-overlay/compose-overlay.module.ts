import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComposeOverlayComponent } from './compose-overlay.component';
import { ComposerModule } from '../composer/composer.module';
import { PersonModule } from '../person/person.module';

@NgModule({
  imports: [
    CommonModule,
    ComposerModule,
    PersonModule
  ],
  declarations: [ComposeOverlayComponent],
  exports: [ComposeOverlayComponent]
})
export class ComposeOverlayModule { }
