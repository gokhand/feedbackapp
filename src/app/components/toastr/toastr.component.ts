import {Component, OnDestroy, OnInit} from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { ToastrConfig } from '../../interfaces/toastr.interface';
import { config } from '../../app.config';

@Component({
  selector: 'app-toastr',
  templateUrl: './toastr.component.html',
  styleUrls: ['./toastr.component.scss']
})
export class ToastrComponent implements OnInit, OnDestroy {

  private subscription: any;
  public config: ToastrConfig;
  public isVisible: boolean;
  private timer: any;

  constructor(private pubSub: PubSubService) { }

  ngOnInit() {
    this.subscription = this.pubSub.$sub('EVT_TOASTR').subscribe((_config: ToastrConfig) => {
      this.config = Object.assign(config.toastrDefaults, _config);
      this.isVisible = true;
      this.timer ? clearTimeout(this.timer) : setTimeout(() => {
        this.isVisible = false;
      }, this.config.timeout);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
