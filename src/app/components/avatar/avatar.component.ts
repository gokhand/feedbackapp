import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../environments/environment';
import {AvatarService} from './avatar.service';
import { DomSanitizer } from '@angular/platform-browser';
const api = environment['api'];

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {

  @Input() hasPhoto: boolean;
  @Input() id: string;
  @Input() className: string;

  public photoUrl: any;

  constructor(private service: AvatarService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    if (this.hasPhoto) { this.getPhoto() };
  }
  getPhoto(): void {
    this.service.get(this.id).subscribe((response) => {
      const arrayBufferView = new Uint8Array(response['_body']);
      const blob = new Blob( [ arrayBufferView ], { type: 'image/jpeg' } );
      const urlCreator = window.URL;
      const url = urlCreator.createObjectURL(blob);
      this.photoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    });
  }

}
