import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AvatarService {

  constructor(private http: Http) { }

  get(id?: string): Observable<object> {
    return this.http.get(`peoplefinder/photo/${id}`, {
      responseType: ResponseContentType.ArrayBuffer,
    }).map((res: Response) => {
      return res;
    });
  }

}
