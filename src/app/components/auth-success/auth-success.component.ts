import {Component, OnDestroy, OnInit} from '@angular/core';
import { AdalService } from 'ng2-adal/core';
import { SessionService } from '../../services/session.service';
import { AppService } from '../../app.service';
import { PubSubService } from 'angular2-pubsub';
import { Router } from '@angular/router';
import { Myuser } from '../../interfaces/myuser.interface';


@Component({
  selector: 'app-auth-success',
  template: '<div></div>'
})
export class AuthSuccessComponent implements OnInit, OnDestroy {

  private subscription1: any;
  private subscription2: any;
  private user: Myuser;

  constructor(
    private adalService: AdalService,
    private sessionService: SessionService,
    private appService: AppService,
    private pubSub: PubSubService,
    private router: Router) {

  }

  ngOnInit() {

    this.subscription1 = this.pubSub.$sub('EVT_AGREED_LEGAL').subscribe(() => {
      this.user.hasAgreedLegal = true;
      this.checkAgreements();
    });

    this.subscription2 = this.pubSub.$sub('EVT_AGREED_PRIVACY').subscribe(() => {
      this.user.hasAgreedPrivacy = true;
      this.checkAgreements();
    });

    const user = this.adalService.userInfo;

    if (user.isAuthenticated) {
      this.fetchUser();
    }
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  fetchUser(): void {
    this.appService.fetch('employee').subscribe((_user: Myuser) => {
      this.sessionService.setUser(_user);
      this.user = _user;
      this.checkAgreements();
    });
  }

  checkAgreements(): void {
    if (this.user.hasAgreedLegal === true && this.user.hasAgreedPrivacy === true) {
      this.router.navigate(['feedback/received']);
    } else {

      if (this.user.hasAgreedLegal === false) {
        this.pubSub.$pub('EVT_SHOW_LEGAL', false);
      }
      if (this.user.hasAgreedPrivacy === false) {
        this.pubSub.$pub('EVT_SHOW_PRIVACY', false);
      }

    }
  }

}
