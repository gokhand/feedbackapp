import {Component, OnInit} from '@angular/core';
import { AdalService } from 'ng2-adal/core';
import { AdalconfigService } from './services/adalconfig.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit {

  constructor(
    private adalService: AdalService,
    private adalConfigService: AdalconfigService) {
    this.adalService.init(this.adalConfigService.adalConfig);
  }

  ngOnInit() {
    this.adalService.handleWindowCallback();
  }

}
