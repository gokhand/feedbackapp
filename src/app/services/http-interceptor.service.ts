import { Injectable } from '@angular/core';
import { ConnectionBackend, RequestOptions, Request, XHRBackend, RequestOptionsArgs, Response, Http, Headers} from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { includes as _includes } from 'lodash';

@Injectable()
export class HttpInterceptorService extends Http {

  private _pendingRequests = 0;
  private _pendingRequestsStatus: Subject<boolean> = new Subject<boolean>();

  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  get pendingRequestsStatus(): Observable<boolean> {
    return this._pendingRequestsStatus.asObservable();
  }

  get pendingRequests(): number {
    return this._pendingRequests;
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {


    this._pendingRequests++;

    if (1 === this._pendingRequests) {
      this._pendingRequestsStatus.next(true);
    }

    return super.request(url, options)
      .map(result => {
        return result;
      })
      .catch(error => {
        console.log(error);
        return Observable.throw(error);
      })
      .finally(() => {
        this._pendingRequests--;

        if (0 === this._pendingRequests) {
          this._pendingRequestsStatus.next(false);
        }
      });
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.get(url, this.getRequestOptionArgs(options));
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.post(url, body, this.getRequestOptionArgs(options));
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.put(url, body, this.getRequestOptionArgs(options));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.delete(url, this.getRequestOptionArgs(options));
  }

  private updateUrl(req: string) {
    return  environment.api + req;
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }

    Object.keys(localStorage).forEach(function(key) {
      if (_includes(key, 'adal.access.token.key') && !options.headers.has('Authorization')) {
        const token = localStorage.getItem(key);
        options.headers.set('Authorization', 'Bearer ' + token);
      }
    });

    options.headers.append('Content-Type', 'application/json');

    return options;
  }
}

export function HttpInterceptorServiceFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
  return new HttpInterceptorService(xhrBackend, requestOptions);
}

export let HttpInterceptorServiceFactoryProvider = {
  provide: Http,
  useFactory: HttpInterceptorServiceFactory,
  deps: [XHRBackend, RequestOptions]
};
