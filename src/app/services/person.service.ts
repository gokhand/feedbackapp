import { Injectable } from '@angular/core';
import { Person } from '../interfaces/person.interface';

@Injectable()
export class PersonService {

  private person: Person;

  public getPerson(): Person {
    return this.person;
  }

  public setPerson(_person: Person): void {
    const personWithId = _person;
    personWithId.ntId = _person.login;
    this.person = personWithId;
  }

}
