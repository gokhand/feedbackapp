import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
const env = environment;

@Injectable()
export class AdalconfigService {
  public get adalConfig(): any {
    return {
      tenant: 'ea80952e-a476-42d4-aaf4-5457852b0f7e',
      clientId: env.adalClientId,
      audience: env.adalAudience,
      redirectUri: window.location.origin + '/',
      postLogoutRedirectUri: window.location.origin + '/',
      cacheLocation: 'localStorage'
    };
  }
}
