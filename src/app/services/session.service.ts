import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdalService } from 'ng2-adal/core';
import { Myuser } from '../interfaces/myuser.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PubSubService } from 'angular2-pubsub';

@Injectable()
export class SessionService {

  private userSubject = new BehaviorSubject<Myuser>({});

  constructor(
    private router: Router,
    private adalService: AdalService,
    private pubSub: PubSubService
  ) { }

  public hasSession(): boolean {
    return this.adalService.userInfo.isAuthenticated;
  }

  public getUser(): Observable<Myuser> {
    return this.userSubject;
  }

  private reportLogin(): void {
    this.pubSub.$pub('EVT_ANALYTICS', {
      'category': 'LOGIN',
      'action': 'login_successful',
      'label': '',
      'value': 0
    });
  }

  public setUser(_user?: Myuser): void {
    this.reportLogin();
    localStorage.setItem('user', JSON.stringify(_user));
    this.userSubject.next(_user);
  }

  public logout(): void {
    this.adalService.logOut();
    this.router.navigate(['auth']).then(() => {
      localStorage.clear();
    });
  }

}
