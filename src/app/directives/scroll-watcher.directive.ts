import { Directive, OnInit, OnDestroy, NgZone, Input, ElementRef } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { config } from '../app.config';

@Directive({
  selector: '[appScrollWatcher]'
})
export class ScrollWatcherDirective implements OnInit, OnDestroy {

  private changeSubject = new Subject();
  private cfg = config.scroll;
  private elem: any;

  constructor(private pubSub: PubSubService, private zone: NgZone, private element: ElementRef) {

  }

  ngOnInit() {
    this.elem = this.element.nativeElement;

    this.zone.runOutsideAngular(() => {
      Observable.fromEvent(this.elem, 'scroll')
        .debounceTime(this.cfg.debounce)
        .distinctUntilChanged()
        .subscribe((e: Event) => {
            this.zone.run(() => {
              this.changeSubject.next(e);
            });
          }
        );
    });
    this.changeSubject.subscribe((e: Event) => { this.emit(e); });
  }

  ngOnDestroy() {
    this.changeSubject.unsubscribe();
  }

  emit(event) {
    if (this.elem.scrollHeight - this.elem.scrollTop - this.cfg.offset <= this.elem.offsetHeight) {
      this.pubSub.$pub('EVT_ELEM_SCROLL_END', event);
    } else {
      this.pubSub.$pub('EVT_ELEM_SCROLLED', event);
    }
  }

}
