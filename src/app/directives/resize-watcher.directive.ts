import { Directive, OnInit, OnDestroy, NgZone } from '@angular/core';
import { PubSubService } from 'angular2-pubsub';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { config } from '../app.config';

@Directive({
  selector: '[appResizeWatcher]'
})
export class ResizeWatcherDirective implements OnInit, OnDestroy {

  private changeSubject = new Subject();
  private cfg = config.resize;

  constructor(private pubSub: PubSubService, private zone: NgZone) {

  }

  ngOnInit() {
    this.zone.runOutsideAngular(() => {
      Observable.fromEvent(window, 'resize')
        .debounceTime(this.cfg.debounce)
        .distinctUntilChanged()
        .subscribe((e: Event) => {
          this.zone.run(() => {
            this.changeSubject.next(e);
          });
        }
      );
    });
    this.changeSubject.subscribe((e: Event) => { this.emit(e); });
  }

  ngOnDestroy() {
    this.changeSubject.unsubscribe();
  }

  emit(event) {
    this.pubSub.$pub('EVT_WINDOW_RESIZED', event);
  }


}
