import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResizeWatcherDirective } from './resize-watcher.directive';
import { RouterWatcherDirective } from './router-watcher.directive';
import { ScrollWatcherDirective } from './scroll-watcher.directive';
import { ScrollPanelDirective } from './scroll-panel.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ResizeWatcherDirective,
    RouterWatcherDirective,
    ScrollWatcherDirective,
    ScrollPanelDirective
  ],
  exports: [
    ResizeWatcherDirective,
    RouterWatcherDirective,
    ScrollWatcherDirective,
    ScrollPanelDirective
  ]
})
export class DirectivesModule { }
