import { Directive, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationStart, RoutesRecognized } from '@angular/router';
import 'rxjs/add/operator/filter';
import { PubSubService } from 'angular2-pubsub';

@Directive({
  selector: '[appRouterWatcher]'
})
export class RouterWatcherDirective implements OnInit, OnDestroy {

  private startSubscription: any;
  private routingSubscription: any;
  private endSubscription: any;

  constructor(private router: Router, private pubSub: PubSubService ) {

  }

  ngOnInit() {
    this.startSubscription = this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event) => {
        this.pubSub.$pub('EVT_NAVIGATION_START', event);
      });
    this.endSubscription = this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((event) => {
        this.pubSub.$pub('EVT_NAVIGATION_END', event);
      });
    this.routingSubscription = this.router.events
        .filter(event => event instanceof RoutesRecognized)
        .subscribe((event) => {
          this.pubSub.$pub('EVT_NAVIGATION_VALID', event);
        });
  }

  ngOnDestroy() {
    this.startSubscription.unsubscribe();
    this.endSubscription.unsubscribe();
    this.routingSubscription.unsubscribe();
  }

}
