import {Directive, ElementRef, Renderer, OnDestroy, OnInit} from '@angular/core';
import {PubSubService} from 'angular2-pubsub';

@Directive({
  selector: '[appScrollPanel]'
})
export class ScrollPanelDirective implements OnInit, OnDestroy {

  private scrollSubscription: any;
  private elem: any;

  constructor(private pubSub: PubSubService, private element: ElementRef, private renderer: Renderer) {
    this.scrollSubscription = this.pubSub.$sub('EVT_WINDOW_RESIZED').subscribe(() => {
      this.updateHeight();
    });
  }
  ngOnInit() {
    this.elem = this.element.nativeElement;
    this.updateHeight();
  }
  ngOnDestroy() {
    this.scrollSubscription.unsubscribe();
  }

  private updateHeight(): void {
    if (this.elem) {
      const tabH = document.getElementById('app_tabs') ? document.getElementById('app_tabs').offsetHeight : 0;
      const actH = document.getElementById('app_actions') ? document.getElementById('app_actions').offsetHeight : 0;
      const braH = document.getElementById('app_branding') ? document.getElementById('app_branding').offsetHeight : 0;
      this.renderer.setElementStyle(this.elem, 'height', `${window.innerHeight - (tabH + actH + braH)}px`);
    }
  }


}
