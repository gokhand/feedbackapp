import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private http: Http) { }

  public update(resource, dto): Observable<object> {
    return this.http.put(`${resource}`, dto)
      .map((res: Response) => res.json());
  }
  public fetch(resource): Observable<object> {
    return this.http.get(`${resource}`)
      .map((res: Response) => res.json());
  }

}
