﻿using System;
using System.Collections.Specialized;
using System.Linq;

namespace Feedback.Common.Extensions
{
    public static class NameValueCollectionExtensions
    {
        public static T GetValue<T>(this NameValueCollection nameValuePairs, string configKey, T defaultValue) where T : IConvertible
        {
            if (nameValuePairs.AllKeys.Contains(configKey))
            {
                string tmpValue = nameValuePairs[configKey];
                return (T)Convert.ChangeType(tmpValue, typeof(T));
            }
            return defaultValue;
        }
    }
}
