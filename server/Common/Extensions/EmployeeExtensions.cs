﻿using Feedback.Common.Models;
using Feedback.Common.Models.Dto;

namespace Feedback.Common.Extensions
{
    public static class EmployeeExtensions
    {
        public static EmployeeDto GetEmployeeDto(this Employee employee)
        {
            EmployeeDto e = new EmployeeDto();
            e.Id = employee.Id;
            e.PeopleFinderId = employee.PeopleFinderId;
            e.NtId = employee.NtId;
            e.FullName = employee.Fullname;
            e.HasPhoto = employee.HasPhoto;
            //e.PhotoUrl = employee.HasPhoto ? $"peoplefinder/photo/{employee.PeopleFinderId}" : "";
            e.Title = employee.Title;
            e.HasAgreedLegal = employee.HasAgreedLegal;
            e.HasAgreedPrivacy = employee.HasAgreedPrivacy;
            return e;
        }

        public static EmployeeLightDto GetEmployeeLightDto(this Employee employee)
        {
            var e = new EmployeeLightDto();
            e.Id = employee.Id;
            e.PeopleFinderId = employee.PeopleFinderId;
            e.NtId = employee.NtId;
            e.FullName = employee.Fullname;
            e.HasPhoto = employee.HasPhoto;
            //e.PhotoUrl = employee.HasPhoto ? $"peoplefinder/photo/{employee.PeopleFinderId}" : "";
            e.Title = employee.Title;
            return e;
        }
    }
}