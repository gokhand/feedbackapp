﻿using System.Diagnostics;

namespace Feedback.Common.Extensions
{
	public static class TraceExtensions
	{
		public static void Debug(string message)
		{
			Trace.WriteLine(message, "DEBUG");
		}
		public static void Info(string message)
		{
			Trace.WriteLine(message, "INFO");
		}

		public static void Warn(string message)
		{
			Trace.WriteLine(message, "WARN");
		}

		public static void Error(string message)
		{
			Trace.WriteLine(message, "ERROR");
		}
	}
}
