﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using Feedback.Common.Extensions;
using Feedback.Common.Models;
using Thread = Feedback.Common.Models;

namespace Feedback.Common.Context
{
    public class FeedbackContext : DbContext
    {
        public FeedbackContext() : base("name=FeedbackContext")
        {
            // Diagnose queries.
            if (ConfigurationManager.AppSettings.GetValue("EntityFrameworkDebug", false))
                Database.Log = s => { Trace.WriteLine(s); };
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Employee> Employees { get; set; }
	    public DbSet<Thread.Thread> Threads { get; set; }
		public DbSet<FeedbackItem> Feedback { get; set; }
	    public DbSet<Setting> Settings { get; set; }
    }
}