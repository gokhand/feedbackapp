﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Feedback.Common.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [MaxLength(255)]
        public string Title { get; set; }
        [MaxLength(255)]
        public string Fullname { get; set; }
        [Index]
        [MaxLength(10)]
        public string NtId { get; set; }
        public int PeopleFinderId { get; set; }
        [MaxLength(255)]
        public string Email { get; set; }
        public bool HasPhoto { get; set; }
        public DateTime LastSignedOn { get; set; }
        public DateTime LastUpdatedFromPeopleFinder { get; set; }
        public bool HasAgreedLegal { get; set; }
        public bool HasAgreedPrivacy { get; set; }
	    public bool NotifyOnFeedback { get; set; }

	    public override string ToString()
	    {
		    return Fullname;
	    }
    }
}