﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Feedback.Common.Models
{
    [Table("Feedback")]
    public class FeedbackItem
    {
        public int Id { get; set; }
        [Index]
        public int ThreadId { get; set; }
        public virtual Thread Thread { get; set; }
        public int GivingEmployeeId { get; set; }
        public virtual Employee GivingEmployee { get; set; }
        public int ReceivingEmployeeId { get; set; }
        public virtual Employee ReceivingEmployee { get; set; }
        public string Message { get; set; }
        [Index]
        public bool IsRead { get; set; }
        [Index]
        public DateTime DateTimeCreated { get; set; }
        public DateTime? DateTimeRead { get; set; }
    }
}