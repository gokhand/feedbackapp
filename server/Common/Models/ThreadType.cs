﻿namespace Feedback.Common.Models
{
    public enum ThreadType : byte
    {
        Given = 0,
        Requested = 1
    }
}