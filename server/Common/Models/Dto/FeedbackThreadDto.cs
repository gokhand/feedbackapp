﻿using System;

namespace Feedback.Common.Models.Dto
{
    public class FeedbackThreadDto
    {
        public int Id { get; set; }
        public int ThreadId { get; set; }
        public int GivingPeopleFinderId { get; set; }
        public int ReceivingPeopleFinderId { get; set; }
        public string Message { get; set; }
        public DateTime DateCreated { get; set; }
	    public DateTime LastUpdated { get; set; }
        public string Status { get; set; }
    }
}