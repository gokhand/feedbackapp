﻿namespace Feedback.Common.Models.Dto
{
	public class AuthenticatedEmployee
	{
		public bool IsAuthenticated { get; set; }
		public Employee Employee { get; set; }
		public string ErrorMessage { get; set; }
	}
}