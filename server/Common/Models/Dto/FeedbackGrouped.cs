﻿using System;
using System.Linq;

namespace Feedback.Common.Models.Dto
{
    public class FeedbackGrouped
    {
        public IOrderedEnumerable<FeedbackItem> OrderedFeedbackByThread { get; set; }
        public DateTime MostRecentFeedback { get; set; }
	    public int UnreadCount { get; set; }
    }
}