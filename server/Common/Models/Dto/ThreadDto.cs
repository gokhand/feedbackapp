﻿using System.Collections.Generic;

namespace Feedback.Common.Models.Dto
{
    public class ThreadDto
    {
        public int ThreadId { get; set; }
        public EmployeeLightDto Employee { get; set; }
        public List<FeedbackThreadDto> Feedbacks { get; set; }
        public int UnreadCount { get; set; }
        public PaginationDto Pagination { get; set; }
    }
}