﻿namespace Feedback.Common.Models.Dto
{
    public class EmployeeDto : EmployeeLightDto
    {
        public bool HasAgreedLegal { get; set; }
        public bool HasAgreedPrivacy { get; set; }
    }
}