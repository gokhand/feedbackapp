﻿namespace Feedback.Common.Models.Dto
{
    public class AggregateDto
    {
        public int GivenUpdate { get; set; }
        public int ReceivedUpdate { get; set; }
    }
}