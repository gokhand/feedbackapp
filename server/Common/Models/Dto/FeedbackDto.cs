﻿using System;

namespace Feedback.Common.Models.Dto
{
    public class FeedbackDto
    {
        public int Id { get; set; }
        public int ThreadId { get; set; }
        public EmployeeLightDto Employee { get; set; }
        public string Message { get; set; }
        public DateTime DateCreated { get; set; }
        public string Status { get; set; }
        public int UnreadCount { get; set; }
    }
}