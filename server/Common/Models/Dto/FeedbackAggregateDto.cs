﻿using System.Collections.Generic;

namespace Feedback.Common.Models.Dto
{
    public class FeedbackAggregateDto
    {
        public AggregateDto Aggregate { get; set; }
        public List<FeedbackDto> Feedbacks { get; set; }
        public PaginationDto Pagination { get; set; }
    }
}