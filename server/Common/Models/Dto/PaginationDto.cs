﻿namespace Feedback.Common.Models.Dto
{
    public class PaginationDto
    {
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public string PrevPageLink { get; set; }
        public string NextPageLink { get; set; }
    }
}