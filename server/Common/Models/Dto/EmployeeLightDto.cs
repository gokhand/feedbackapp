﻿namespace Feedback.Common.Models.Dto
{
    public class EmployeeLightDto
    {
        public int Id { get; set; }
        public string NtId { get; set; }
        public int PeopleFinderId { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public bool HasPhoto { get; set; }
        //public string PhotoUrl { get; set; }
    }
}