﻿using System;
using System.Collections.Generic;

namespace Feedback.Common.Models
{
    public class Thread
    {
        public int Id { get; set; }
        public int InstigatingEmployeeId { get; set; }
        public virtual Employee InstigatingEmployee { get; set; }
        public int ReceivingEmployeeId { get; set; }
        public virtual Employee ReceivingEmployee { get; set; }
        public ThreadType ThreadType { get; set; }
        public DateTime DateTimeCreated { get; set; }
	    public DateTime LastUpdated { get; set; }
	    public virtual ICollection<FeedbackItem> FeedbackItems { get; set; }
    }
}