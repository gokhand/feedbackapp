﻿using System;

namespace Feedback.Common.Models
{
	public class Setting
	{
		public int Id { get; set; }
		public string From { get; set; }
		public string Subject { get; set; }
		public string Content { get; set; }
		public DateTime DateTime { get; set; }
	}
}
