﻿using log4net.Appender;

namespace Feedback.Common.Diagnostics
{
	public class ForceableRollingFileAppender : RollingFileAppender
	{
		private bool _rollBeforeNextAppend;

		public void Roll()
		{
			_rollBeforeNextAppend = true;
		}

		protected override void AdjustFileBeforeAppend()
		{
			if (!_rollBeforeNextAppend)
			{
				base.AdjustFileBeforeAppend();
				return;
			}

			CloseFile();
			RollOverRenameFiles(File);
			SafeOpenFile(File, false);
			_rollBeforeNextAppend = false;
		}
	}
}
