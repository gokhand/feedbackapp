﻿using System.Diagnostics;
using log4net;

namespace Feedback.Common.Diagnostics
{
	public class Log4netTraceListener : TraceListener
	{
		private readonly ILog _log;

		public Log4netTraceListener()
		{
			_log = LogManager.GetLogger("StationManagement");
		}

		public override void Write(string message)
		{
			try
			{
				_log.Debug(message);
			}
			catch
			{
				// ignored
			}
		}

		public override void Write(string message, string category)
		{
			WriteLine(message, category);
		}

		public override void WriteLine(string message)
		{
			try
			{
				_log.Debug(message);
			}
			catch
			{
				// ignored
			}
		}

		public override void WriteLine(string message, string category)
		{
			switch (category)
			{
				case "DEBUG":
					_log.Debug(message);
					break;
				case "INFO":
					_log.Info(message);
					break;
				case "WARN":
					_log.Warn(message);
					break;
				case "ERROR":
					_log.Error(message);
					break;
			}
		}
	}
}
