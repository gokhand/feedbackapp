using System;
using System.Data.Entity.Migrations;
using Feedback.Common.Context;
using Feedback.Common.Models;

namespace Feedback.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<FeedbackContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Feedback.Context.FeedbackContext";
        }

        protected override void Seed(FeedbackContext context)
        {
            // Basic test data
            var initialDate = new DateTime(1970, 1, 1);

            context.Employees.AddOrUpdate(x => x.Id,
                new Employee { Id = 1, Email = "ellen@ripley.com", Fullname = "Ripley, Ellen", Title = "Lieutenant", NtId = "DUNN0", HasPhoto = false, LastSignedOn = initialDate, LastUpdatedFromPeopleFinder = initialDate, HasAgreedLegal = true, HasAgreedPrivacy = true, PeopleFinderId = 111, NotifyOnFeedback = true },
                new Employee { Id = 2, Email = "dwayne@hicks.com", Fullname = "Hicks, Dwayne", Title = "Corporal", NtId = "DUNN1", HasPhoto = false, LastSignedOn = initialDate, LastUpdatedFromPeopleFinder = initialDate, HasAgreedLegal = true, HasAgreedPrivacy = true, PeopleFinderId = 222, NotifyOnFeedback = true },
                new Employee { Id = 3, Email = "bill@hudson.com", Fullname = "Hudson, William", Title = "Private", NtId = "DUNN2", HasPhoto = false, LastSignedOn = initialDate, LastUpdatedFromPeopleFinder = initialDate, HasAgreedLegal = false, HasAgreedPrivacy = false, PeopleFinderId = 333, NotifyOnFeedback = false },
                new Employee { Id = 4, Email = "carter@burke.com", Fullname = "Burke, Carter", Title = "Company Man", NtId = "DUNN3", HasPhoto = false, LastSignedOn = initialDate, LastUpdatedFromPeopleFinder = initialDate, HasAgreedLegal = false, HasAgreedPrivacy = false, PeopleFinderId = 444, NotifyOnFeedback = true },
                new Employee { Id = 5, Email = "al@apone.com", Fullname = "Apone, Al", Title = "Sergeant", NtId = "DUNN4", HasPhoto = false, LastSignedOn = initialDate, LastUpdatedFromPeopleFinder = initialDate, HasAgreedLegal = false, HasAgreedPrivacy = false, PeopleFinderId = 555, NotifyOnFeedback = false }
                );

            initialDate = DateTime.Now.AddDays(-10);

            context.Threads.AddOrUpdate(x => x.Id,
                new Thread { Id = 1, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 1, ReceivingEmployeeId = 2, ThreadType = ThreadType.Given },
                new Thread { Id = 2, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 1, ReceivingEmployeeId = 3, ThreadType = ThreadType.Requested },
                new Thread { Id = 3, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 1, ReceivingEmployeeId = 4, ThreadType = ThreadType.Given },
                new Thread { Id = 4, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 2, ReceivingEmployeeId = 1, ThreadType = ThreadType.Given },
                new Thread { Id = 5, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 1, ReceivingEmployeeId = 3, ThreadType = ThreadType.Requested },
                new Thread { Id = 6, DateTimeCreated = initialDate, LastUpdated = initialDate, InstigatingEmployeeId = 1, ReceivingEmployeeId = 4, ThreadType = ThreadType.Given }
            );

            context.Feedback.AddOrUpdate(x => x.Id,
                new FeedbackItem { Id = 1, ThreadId = 1, DateTimeCreated = initialDate, GivingEmployeeId = 1, ReceivingEmployeeId = 2, Message = "The 1st given feedback" },
                new FeedbackItem { Id = 2, ThreadId = 2, DateTimeCreated = initialDate.AddHours(1), GivingEmployeeId = 1, ReceivingEmployeeId = 3, Message = "The 1st requested feedback" },
                new FeedbackItem { Id = 3, ThreadId = 3, DateTimeCreated = initialDate.AddHours(2), GivingEmployeeId = 1, ReceivingEmployeeId = 4, Message = "The 2nd given feedback" },
                new FeedbackItem { Id = 4, ThreadId = 1, DateTimeCreated = initialDate.AddHours(3), GivingEmployeeId = 2, ReceivingEmployeeId = 1, Message = "The 1st response" },
                new FeedbackItem { Id = 5, ThreadId = 2, DateTimeCreated = initialDate.AddHours(4), GivingEmployeeId = 3, ReceivingEmployeeId = 1, Message = "The 2nd response" },
                new FeedbackItem { Id = 6, ThreadId = 3, DateTimeCreated = initialDate.AddHours(5), GivingEmployeeId = 4, ReceivingEmployeeId = 1, Message = "The 3rd response" },

                new FeedbackItem { Id = 7, ThreadId = 4, DateTimeCreated = initialDate.AddHours(6), GivingEmployeeId = 2, ReceivingEmployeeId = 1, Message = "New Given Thread" },
                new FeedbackItem { Id = 8, ThreadId = 5, DateTimeCreated = initialDate.AddHours(7), GivingEmployeeId = 1, ReceivingEmployeeId = 3, Message = "Newer Requested Thread" },
                new FeedbackItem { Id = 9, ThreadId = 6, DateTimeCreated = initialDate.AddHours(8), GivingEmployeeId = 1, ReceivingEmployeeId = 4, Message = "Newest Given Thread" },
                new FeedbackItem { Id = 10, ThreadId = 4, DateTimeCreated = initialDate.AddHours(9), GivingEmployeeId = 1, ReceivingEmployeeId = 2, Message = "The 1st response" },
                new FeedbackItem { Id = 11, ThreadId = 5, DateTimeCreated = initialDate.AddHours(10), GivingEmployeeId = 3, ReceivingEmployeeId = 1, Message = "The 2nd response" },
                new FeedbackItem { Id = 12, ThreadId = 6, DateTimeCreated = initialDate.AddHours(11), GivingEmployeeId = 4, ReceivingEmployeeId = 1, Message = "The 3rd response" },

                new FeedbackItem { Id = 13, ThreadId = 1, DateTimeCreated = initialDate.AddHours(12), GivingEmployeeId = 2, ReceivingEmployeeId = 1, Message = "3rd" },
                new FeedbackItem { Id = 14, ThreadId = 2, DateTimeCreated = initialDate.AddHours(13), GivingEmployeeId = 3, ReceivingEmployeeId = 1, Message = "3rd again" },
                new FeedbackItem { Id = 15, ThreadId = 3, DateTimeCreated = initialDate.AddHours(14), GivingEmployeeId = 4, ReceivingEmployeeId = 1, Message = "3rd again again" }
                );

			context.Settings.AddOrUpdate(x => x.Id,
				new Setting { Id = 1,
					From = "echo@bp.com",
					Subject = "You got mail",
					Content = @"Hello {username}

					You�ve had some feedback activity in echo today � please login and take a look : {website}

					Regards

					The echo team
					", DateTime = DateTime.Now.Date.AddDays(-2).AddHours(7) });
        }
    }
}
