﻿using System.Collections.Generic;

namespace Feedback.Models.PeopleFinder
{
    public class Site
    {
        public string IsActive { get; set; }
        public string LocationCode { get; set; }
        public string BuildingName { get; set; }
        public string PostCode { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string AddressLine2 { get; set; }
        public string SiteName { get; set; }
        public string AddressLine1 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string BuildingId { get; set; }
    }

    public class Detail
    {
        public string EndUserId { get; set; }
        public object GpdSpuCode { get; set; }
        public string PhotoModifiedDate { get; set; }
        public string GpdBuName { get; set; }
        public string GpdSpuName { get; set; }
        public string GpdBuCode { get; set; }
        public string Department { get; set; }
        public string GpdSegmentCode { get; set; }
        public string SponsorOrManagerPrimaryLogin { get; set; }
        public string PrimaryEmail { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public bool HasPhoto { get; set; }
        public string GpdSegmentName { get; set; }
        public string SponsorOrManagerFullName { get; set; }
        public string AlternateFullName { get; set; }
        public string PrimaryLogin { get; set; }
        public string Notes { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string BuildingId { get; set; }
    }

    public class Datum
    {
        public Site Site { get; set; }
        public List<object> Reports { get; set; }
        public Detail Detail { get; set; }
    }

    public class ByIdResult
    {
        public string Status { get; set; }
        public string Query { get; set; }
        public List<Datum> Data { get; set; }
    }
}