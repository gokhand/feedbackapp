﻿using System.Collections.Generic;

namespace Feedback.Models.PeopleFinder
{

    public class PhotoDetail
    {
        public string EndUserId { get; set; }
        public string Photo { get; set; }
        public long PhotoModifiedDate { get; set; }
        public List<string> EntityFieldsInOrder { get; set; }
        public object SearchResultFields { get; set; }
    }

    public class PhotoResult
    {
        public string Status { get; set; }
        [System.Xml.Serialization.XmlElementAttribute("Data", typeof(PhotoDetail), IsNullable = true)]
        public PhotoDetail Data { get; set; }
    }
}