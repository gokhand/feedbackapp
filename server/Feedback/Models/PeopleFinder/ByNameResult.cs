﻿using System.Collections.Generic;

namespace Feedback.Models.PeopleFinder
{

    public class PersonDetail
    {
        public string Id { get; set; }
        public bool HasPhoto { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public int NumberOfReports { get; set; }
        public string Login { get; set; }
        public string Dept { get; set; }
        public string FullName { get; set; }
        public string Type { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }

    public class ByNameResult
    {
        public string Timestamp { get; set; }
        public string Status { get; set; }
        public string Query { get; set; }
        public int PersonDetailsNumberOfHits { get; set; }
        [System.Xml.Serialization.XmlArrayItem("data", typeof(PersonDetail), IsNullable = false)]
        public List<PersonDetail> Data { get; set; }
        public int TotalNumberOfHits { get; set; }
        public int MaxHits { get; set; }
        public string LoggedInUser { get; set; }
        public int SiteNumberOfHits { get; set; }
    }
}