﻿using System.Configuration;
using System.Web.Http;
using Feedback.Common.Extensions;
using Newtonsoft.Json.Serialization;

namespace Feedback
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static bool _enableHttp;

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            
            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            _enableHttp = ConfigurationManager.AppSettings.GetValue("EnableHttp", false);
        }

        //protected void Application_Start(object sender, EventArgs e)
        //{
        //    if (_enableHttp) return;

        //    if (!Request.IsLocal)
        //    {
        //        switch (Request.Url.Scheme)
        //        {
        //            case "https":
        //                Response.AddHeader("Strict-Transport-Security", "max-age=300");
        //                break;
        //            case "http":
        //                var path = "https://" + Request.Url.Host + Request.Url.PathAndQuery;
        //                Response.Status = "301 Moved Permanently";
        //                Response.AddHeader("Location", path);
        //                break;
        //        }
        //    }
        //}
    }
}
