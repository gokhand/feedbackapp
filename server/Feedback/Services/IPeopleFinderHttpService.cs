﻿using System.Net.Http;
using System.Threading.Tasks;
using Feedback.Models.PeopleFinder;

namespace Feedback.Services
{
    public interface IPeopleFinderHttpService
    {
        HttpClient HttpClient { get; }
        string BuildRequest(string request);
        int Timeout { get; }
        Task<ByIdResult> GetById(string id);
        Task<ByNameResult> GetByEmail(string emailAddress);
    }
}