﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Feedback.Common.Extensions;
using Feedback.Models.PeopleFinder;
using Newtonsoft.Json;

namespace Feedback.Services
{
    public sealed class PeopleFinderHttpService : IPeopleFinderHttpService, IDisposable
    {
        private readonly string _peopleFinderApiPath = ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpointPath", "");

        public int Timeout { get; } = ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpointTimeoutMilliseconds", 1000);
        
        public HttpClient HttpClient { get; }
        
        public PeopleFinderHttpService()
        {
            HttpClient = new HttpClient();
            HttpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpoint", ""));
            HttpClient.Timeout = new TimeSpan(0, 0, 0, 0, Timeout);

            var username = ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpointUsername", "");
            var password = ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpointPassword", "");

            string authorization = $"{Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"))}";

            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authorization);
            HttpClient.DefaultRequestHeaders.Add("x-api-key", ConfigurationManager.AppSettings.GetValue("PeopleFinderEndpointApiKey", ""));
            
            // todo - service should have a valid cert that we trust - for testing
            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }

        public string BuildRequest(string request)
        {
            return $"/{_peopleFinderApiPath}/{request}";
        }

        public async Task<ByIdResult> GetById(string id)
        {
            try
            {
                string request = BuildRequest($"search/byid/{id}");
                HttpResponseMessage response = await HttpClient.GetAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    ByIdResult byIdResult = JsonConvert.DeserializeObject<ByIdResult>(result);
                    return byIdResult;
                }

                throw new AuthenticationException();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                throw;
            }
        }

        public async Task<ByNameResult> GetByEmail(string emailAddress)
        {
            try
            {
                string request = BuildRequest("search/byemail");

                var content = new StringContent(@"{ ""addresses"":[""" + emailAddress + @"""]}", Encoding.UTF8, "application/json");

                HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                HttpResponseMessage response = await HttpClient.PostAsync(request, content);
                HttpClient.DefaultRequestHeaders.Remove("accept");

                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    ByNameResult byIdResult = JsonConvert.DeserializeObject<ByNameResult>(result);
                    return byIdResult;
                }

                throw new AuthenticationException();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                throw;
            }
        }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }
}