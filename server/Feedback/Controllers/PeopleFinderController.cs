﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Feedback.Authentication;
using Feedback.Models.PeopleFinder;
using Feedback.Services;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace Feedback.Controllers
{
    public class PeopleFinderController : AuthenticatedApiController
    {
        private readonly IPeopleFinderHttpService _peopleFinderService = new PeopleFinderHttpService();
        private readonly IPeopleFinderHttpService _peopleFinderImageService = new PeopleFinderHttpService();

        [HttpGet]
        [Route("api/peoplefinder/search/{name}")]
        [SwaggerResponse(HttpStatusCode.OK, "Default Model directly from PeopleFinder API", typeof(ByNameResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        public async Task<IHttpActionResult> SearchPeople(string name, int max)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return BadRequest(authenticatedEmployee.ErrorMessage);

			try
			{
				// Remove leading and trailing spaces as peoplefinder no like
				name = name.Trim();

                string request = _peopleFinderService.BuildRequest($"search/name/{name}?max={max}");

                HttpResponseMessage response = await _peopleFinderService.HttpClient.GetAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    using (var sr = new StreamReader(await response.Content.ReadAsStreamAsync()))
                    {
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            var serializer = new JsonSerializer();
                            var result = serializer.Deserialize<ByNameResult>(reader);
                            return Ok(result);
                        }
                    }
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    return BadRequest("The internal account used to authenticate PeopleFinder calls was rejected.");

                return BadRequest($"PeopleFinder API failed with this message: {response.ReasonPhrase}");
            }
            catch (TaskCanceledException taskCanceledException)
            {
                if (!taskCanceledException.CancellationToken.IsCancellationRequested)
                    return BadRequest($"Timed out after {_peopleFinderService.Timeout}ms calling PeopleFinder.");

                return BadRequest("People finder call was cancelled.");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }
            finally
            {
                _peopleFinderService.HttpClient.CancelPendingRequests();
            }
        }

        [HttpGet]
        [Route("api/peoplefinder/photo/{peopleFinderId}")]
        [SwaggerResponse(HttpStatusCode.OK, "Image - This will fail in swagger with access denied")]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Typically authentication failure.")]
        public async Task<HttpResponseMessage> GetPhoto(string peopleFinderId)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = $"No authenticated employee found. {authenticatedEmployee.ErrorMessage}" };

            try
            {
                string request = _peopleFinderService.BuildRequest($"photo/{peopleFinderId}");
                HttpResponseMessage response = await _peopleFinderImageService.HttpClient.GetAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using (var sr = new StreamReader(await response.Content.ReadAsStreamAsync()))
                    {
                        using (JsonReader reader = new JsonTextReader(sr))
                        {
                            var serializer = new JsonSerializer();
                            var result = serializer.Deserialize<PhotoResult>(reader);

                            if (result.Data == null)
                                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Unable to retrieve photo - no data" };

                            if (result.Data.Photo == null)
                                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Unable to retrieve photo - no image" };

                            HttpResponseMessage imageResponse = new HttpResponseMessage(HttpStatusCode.OK);
                            imageResponse.Content = new ByteArrayContent(Convert.FromBase64String(result.Data.Photo));
                            imageResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                            return imageResponse;
                        }
                    }
                }

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "The internal account used to authenticate PeopleFinder calls was rejected." };
                    
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = response.ReasonPhrase };
            }
            catch (TaskCanceledException taskCanceledException)
            {
                if (!taskCanceledException.CancellationToken.IsCancellationRequested)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = $"Timed out after {_peopleFinderImageService.Timeout}ms calling PeopleFinder." };

                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "People finder call was cancelled." };
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = e.Message };
            }
            finally
            {
                _peopleFinderImageService.HttpClient.CancelPendingRequests();
            }
        }
    }
}