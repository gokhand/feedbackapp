﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Feedback.Authentication;
using Feedback.Common.Extensions;
using Feedback.Common.Models;
using Feedback.Common.Models.Dto;
using Feedback.Services;
using Swashbuckle.Swagger.Annotations;

namespace Feedback.Controllers
{
    public class FeedbackController : AuthenticatedApiController
    {
        private readonly IPeopleFinderHttpService _peopleFinderHttpService;

        public FeedbackController(IPeopleFinderHttpService peopleFinderHttpService)
        {
            _peopleFinderHttpService = peopleFinderHttpService;
        }

        [HttpGet]
        [Route("api/feedback/thread/{threadId:int}", Name = "thread")]
        [SwaggerResponse(HttpStatusCode.OK, "Feedback Thread", typeof(ThreadDto))]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> GetThread(int threadId, int page, int pageSize)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return BadRequest(authenticatedEmployee.ErrorMessage);
			
			try
			{
				// Get the thread
				Thread thread = await Db.Threads.FindAsync(threadId);
                if (thread == null)
                    return BadRequest($"Unable to find original Thread with id {threadId}");

                // Only employee involved in original thread may request it.
                if (authenticatedEmployee.Employee.Id != thread.InstigatingEmployeeId && authenticatedEmployee.Employee.Id != thread.ReceivingEmployeeId)
                    return BadRequest("Only the two original employees on this thread may request it.");

                var threadDto = new ThreadDto
                {
                    Feedbacks = new List<FeedbackThreadDto>(),
                    ThreadId = threadId
                };

                // Set the thread employee to the employee who didn't request this thread to read.
                threadDto.Employee =
                    thread.ReceivingEmployeeId != authenticatedEmployee.Employee.Id
                        ? thread.ReceivingEmployee.GetEmployeeLightDto()
                        : thread.InstigatingEmployee.GetEmployeeLightDto();

                IQueryable<FeedbackItem> threadFeedback = from f in Db.Feedback
                    where f.ThreadId == threadId
                    select f;

                // Get total amount of unread feedback in the thread
                threadDto.UnreadCount = threadFeedback.Count(x => !x.IsRead);

				// Yo, paginate!
                threadDto.Pagination = AddPagination(threadFeedback.Count(), page, pageSize, "thread");

                // Select request page of items
                List<FeedbackItem> pagedThreadItems =
                    threadFeedback
                        .OrderBy(x => x.DateTimeCreated)
                        .Skip(pageSize * page)
                        .Take(pageSize)
                        .ToList();

                foreach (var feedbackItem in pagedThreadItems)
                {
                    var feedbackDto = new FeedbackThreadDto();

                    feedbackDto.Id = feedbackItem.Id;
                    feedbackDto.ThreadId = feedbackItem.ThreadId;
                    feedbackDto.Message = feedbackItem.Message;
                    feedbackDto.Status = feedbackItem.IsRead ? "Read" : "Unread";
                    feedbackDto.DateCreated = feedbackItem.DateTimeCreated;
					feedbackDto.GivingPeopleFinderId = feedbackItem.GivingEmployee.PeopleFinderId;
                    feedbackDto.ReceivingPeopleFinderId = feedbackItem.ReceivingEmployee.PeopleFinderId;
	                threadDto.Feedbacks.Add(feedbackDto);

					// Mark any item unread as read if the receiver is the user doing the thread/get
					if (!feedbackItem.IsRead)
					{
						if (feedbackItem.ReceivingEmployeeId != authenticatedEmployee.Employee.Id)
							continue;

						try
						{
							feedbackItem.IsRead = true;
							feedbackItem.DateTimeRead = DateTime.Now;
							Db.Entry(feedbackItem).State = EntityState.Modified;

							await Db.SaveChangesAsync();
						}
						catch (Exception e)
						{
							Trace.WriteLine(e);
						}
					}
                }

                return Ok(threadDto);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("api/feedback/received", Name = "feedbackreceived")]
        [SwaggerResponse(HttpStatusCode.OK, "FeedbackAggregate", typeof(FeedbackAggregateDto))]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> GetFeedbackReceived(int page, int pageSize)
        {
            return await GetFeedback(ThreadType.Requested, page, pageSize);
        }

        [HttpGet]
        [Route("api/feedback/given", Name = "feedbackgiven")]
        [SwaggerResponse(HttpStatusCode.OK, "FeedbackAggregate", typeof(FeedbackAggregateDto))]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> GetFeedbackGiven(int page, int pageSize)
        {
            return await GetFeedback(ThreadType.Given, page, pageSize);
        }
        
        private async Task<IHttpActionResult> GetFeedback(ThreadType threadType, int page, int pageSize)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return BadRequest(authenticatedEmployee.ErrorMessage);

			try
			{
				// Get totals used for paging which can be reused for aggregates
				var feedbackAggregateDto = new FeedbackAggregateDto
				{
					Aggregate = new AggregateDto
					{
						GivenUpdate = 0,
						ReceivedUpdate = 0
					}
				};
				
				List<FeedbackDto> feedbackDtos = new List<FeedbackDto>();

                // My Feedback - Total unread threads
				IQueryable<FeedbackGrouped> requestedGroupedItems = from t in Db.Threads
					where
						(t.ThreadType == ThreadType.Given && t.ReceivingEmployeeId == authenticatedEmployee.Employee.Id)
						||
						(t.ThreadType == ThreadType.Requested && t.InstigatingEmployeeId == authenticatedEmployee.Employee.Id)
					orderby t.LastUpdated descending
					select
					new FeedbackGrouped
						{
							OrderedFeedbackByThread = t.FeedbackItems.OrderByDescending(x => x.DateTimeCreated),
							MostRecentFeedback = t.LastUpdated,
							UnreadCount = t.FeedbackItems.Count(u => !u.IsRead)
						};
				feedbackAggregateDto.Aggregate.ReceivedUpdate = requestedGroupedItems.Count(x => x.UnreadCount > 0);

				// Given Feedback - Total unread threads
				IQueryable<FeedbackGrouped> givenGroupedItems = from t in Db.Threads
					where
						(t.ThreadType == ThreadType.Given && t.InstigatingEmployeeId == authenticatedEmployee.Employee.Id)
						|| 
						(t.ThreadType == ThreadType.Requested && t.ReceivingEmployeeId == authenticatedEmployee.Employee.Id)
					orderby t.LastUpdated descending
					select
					new FeedbackGrouped
					{
						OrderedFeedbackByThread = t.FeedbackItems.OrderByDescending(x => x.DateTimeCreated),
						MostRecentFeedback = t.LastUpdated,
						UnreadCount = t.FeedbackItems.Count(u => !u.IsRead)
					};
				feedbackAggregateDto.Aggregate.GivenUpdate = givenGroupedItems.Count(x => x.UnreadCount > 0);
				
				// Return most recent feedback item for each thread & use pagination
				IQueryable<FeedbackGrouped> groupedItems;
				switch (threadType)
				{
					case ThreadType.Given:
						groupedItems = givenGroupedItems;

						// Perform pagination
						feedbackAggregateDto.Pagination = AddPagination(givenGroupedItems.Count(), page, pageSize, "feedbackgiven");
						break;

					case ThreadType.Requested:
						groupedItems = requestedGroupedItems;

						// Perform pagination
						feedbackAggregateDto.Pagination = AddPagination(requestedGroupedItems.Count(), page, pageSize, "feedbackreceived");
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(threadType), threadType, null);
				}

                // Select request page of items
                List<FeedbackGrouped> pagedFeedbackItems = 
                    groupedItems
                        .OrderByDescending(x => x.MostRecentFeedback)
                        .Skip(pageSize * page)
                        .Take(pageSize)
                        .ToList();

                foreach (var groupedFeedbackItem in pagedFeedbackItems)
                {
                    var feedbackDto = new FeedbackDto();

                    var feedbackItem = groupedFeedbackItem.OrderedFeedbackByThread.FirstOrDefault();
                    if (feedbackItem == null)
                        continue;

                    feedbackDto.UnreadCount = groupedFeedbackItem.OrderedFeedbackByThread.Count(x => !x.IsRead);

                    feedbackDto.Id = feedbackItem.Id;
                    feedbackDto.Message = feedbackItem.Message;
                    feedbackDto.Status = feedbackItem.IsRead ? "Read" : "Unread";
                    feedbackDto.DateCreated = feedbackItem.DateTimeCreated;

					// We now always return the person other than the one requesting as the employee. Just so we don't match any other chat system in the world.
	                //feedbackDto.Employee = threadType == ThreadType.Given ? feedbackItem.ReceivingEmployee.GetEmployeeLightDto() : feedbackItem.GivingEmployee.GetEmployeeLightDto();
					feedbackDto.Employee =
		                feedbackItem.ReceivingEmployeeId != authenticatedEmployee.Employee.Id
			                ? feedbackItem.ReceivingEmployee.GetEmployeeLightDto()
			                : feedbackItem.GivingEmployee.GetEmployeeLightDto();
					
                    feedbackDto.ThreadId = feedbackItem.ThreadId;

                    feedbackDtos.Add(feedbackDto);
                }

                feedbackAggregateDto.Feedbacks = feedbackDtos;

                return Ok(feedbackAggregateDto);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("api/feedback/give", Name = "feedback/give")]
        [SwaggerOperation("Create")]
        [SwaggerResponse(HttpStatusCode.Created)]
        public async Task<IHttpActionResult> GiveFeedback(FeedbackDto feedbackDto)
        {
            return await PostFeedback(feedbackDto, ThreadType.Given, "give");
        }

        [HttpPost]
        [Route("api/feedback/request", Name = "feedback/request")]
        [SwaggerOperation("Create")]
        [SwaggerResponse(HttpStatusCode.Created)]
        public async Task<IHttpActionResult> RequestFeedback(FeedbackDto feedbackDto)
        {
            return await PostFeedback(feedbackDto, ThreadType.Requested, "request");
        }

        private async Task<IHttpActionResult> PostFeedback(FeedbackDto feedbackDto, ThreadType threadType, string route)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return BadRequest(authenticatedEmployee.ErrorMessage);

            if (feedbackDto == null)
                return BadRequest("No valid feedback details supplied.");

            var feedback = new FeedbackItem();

            try
            {
                // Validate
                if (string.IsNullOrEmpty(feedbackDto.Employee?.NtId))
                    return BadRequest("Please supply a valid NtId to store your feedback against.");

                if (authenticatedEmployee.Employee.NtId.Equals(feedbackDto.Employee.NtId, StringComparison.CurrentCultureIgnoreCase))
                    return BadRequest("You can't post feedback to yourself.");
                
                if (string.IsNullOrWhiteSpace(feedbackDto.Message))
                    return BadRequest("You must supply a message to post.");

                // Find the Employee feeback is being given for
                Employee existingEmployee = await Db.Employees.FirstOrDefaultAsync(x => x.NtId == feedbackDto.Employee.NtId);
                if (existingEmployee == null)
                {
                    // We need to get the employee details from People Finder
                    try
                    {
                        var byIdResult = await _peopleFinderHttpService.GetById(feedbackDto.Employee.NtId);
                        if (byIdResult?.Data == null || !byIdResult.Data.Any())
                            return BadRequest($"Unable to retrieve Employee {feedbackDto.Employee.NtId} from people finder.");

                        Models.PeopleFinder.Detail detail = byIdResult.Data[0].Detail;

                        // Save the employee
	                    existingEmployee = await AddEmployee(detail, false);
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e);
                        return BadRequest($"Unable to retrieve Employee {feedbackDto.Employee.NtId} from people finder.");
                    }
                }

                feedback.Thread = new Thread
                {
                    InstigatingEmployeeId = authenticatedEmployee.Employee.Id,
                    ReceivingEmployeeId = existingEmployee.Id,
                    ThreadType = threadType,
                    DateTimeCreated = DateTime.Now,
					LastUpdated = DateTime.Now
                };
                
                feedback.GivingEmployeeId = authenticatedEmployee.Employee.Id;
                feedback.ReceivingEmployeeId = existingEmployee.Id;
                feedback.DateTimeCreated = DateTime.Now;
                feedback.Message = feedbackDto.Message;
                
                Db.Feedback.Add(feedback);
                await Db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }

            return CreatedAtRoute($"feedback/{route}", new { id = feedback.ThreadId }, feedback.ThreadId );
        }

        [HttpPut]
        [Route("api/feedback/reply")]
        [ResponseType(typeof(void))]
        [SwaggerOperation("Update")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> PutFeedback(int threadId, FeedbackDto feedbackDto)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee();
	        if (!authenticatedEmployee.IsAuthenticated)
		        return BadRequest(authenticatedEmployee.ErrorMessage);

			var feedback = new FeedbackItem();

            try
            {
                // Validate
                if (string.IsNullOrEmpty(feedbackDto.Employee?.NtId))
                    return BadRequest("Please supply a valid NtId to store your feedback against.");

                if (string.IsNullOrWhiteSpace(feedbackDto.Message))
                    return BadRequest("You must supply a message to put.");

                // Get the thread
                var thread = await Db.Threads.FindAsync(threadId);
                if (thread == null)
                    return BadRequest($"Unable to find original Thread with id {threadId}");
                
                // Only employee involved in original thread may request it.
                if (authenticatedEmployee.Employee.Id != thread.InstigatingEmployeeId && authenticatedEmployee.Employee.Id != thread.ReceivingEmployeeId)
                    return BadRequest("Only the two original employees on this thread may add to it.");

                // Find the Employee feeback is being given for
                Employee existingEmployee = await Db.Employees.FirstOrDefaultAsync(x => x.NtId == feedbackDto.Employee.NtId);
                if (existingEmployee == null)
                    return BadRequest($"Unable to retrieve Employee {feedbackDto.Employee.NtId}.");

				// Update thread to indicate when last added to.
				thread.LastUpdated = DateTime.Now;
				Db.Entry(thread).State = EntityState.Modified;

                feedback.ThreadId = threadId;
                feedback.GivingEmployeeId = authenticatedEmployee.Employee.Id;
                feedback.ReceivingEmployeeId = existingEmployee.Id;
                feedback.DateTimeCreated = DateTime.Now;
                feedback.Message = feedbackDto.Message;
                
                Db.Feedback.Add(feedback);
                await Db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}