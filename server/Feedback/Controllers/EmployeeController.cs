﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Feedback.Authentication;
using Feedback.Common.Extensions;
using Feedback.Common.Models.Dto;
using Swashbuckle.Swagger.Annotations;

namespace Feedback.Controllers
{
    public class EmployeeController : AuthenticatedApiController
    {
        [HttpGet]
        [Route("api/employee")]
        [SwaggerResponse(HttpStatusCode.OK, "Employee", typeof(EmployeeDto))]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> GetEmployee()
        {
            try
            {
                var authenticatedEmployee = await GetAuthenticatedEmployee(true, false);
                if (!authenticatedEmployee.IsAuthenticated)
                    return BadRequest(authenticatedEmployee.ErrorMessage);

                var employeeDto = authenticatedEmployee.Employee.GetEmployeeDto();
                return Ok(employeeDto);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }   
        }

        [HttpPut]
        [Route("api/employee/agreedlegal")]
        [ResponseType(typeof(void))]
        [SwaggerOperation("Update")]
        [SwaggerResponse(HttpStatusCode.OK, "We only consume HasAgreedLegal")]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public async Task<IHttpActionResult> PutEmployee(int id, EmployeeDto employeeDto)
        {
	        var authenticatedEmployee = await GetAuthenticatedEmployee(false, false);
            if (!authenticatedEmployee.IsAuthenticated)
				return BadRequest(authenticatedEmployee.ErrorMessage);
         
            try
            {
                if (authenticatedEmployee.Employee.Id != id)
                    return BadRequest("The employee you're attempting to update isn't the authenticated employee");

                var existingEmployee = await Db.Employees.FindAsync(authenticatedEmployee.Employee.Id);
                if (existingEmployee == null)
                    return NotFound();

                existingEmployee.LastSignedOn = DateTime.Now;
                existingEmployee.HasAgreedLegal = employeeDto.HasAgreedLegal;

                Db.Entry(existingEmployee).State = EntityState.Modified;

                await Db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                return BadRequest(e.Message);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

	    [HttpPut]
	    [Route("api/employee/agreedprivacy")]
	    [ResponseType(typeof(void))]
	    [SwaggerOperation("Update")]
	    [SwaggerResponse(HttpStatusCode.OK, "We only consume HasAgreedPrivacy")]
	    [SwaggerResponse(HttpStatusCode.NotFound)]
	    public async Task<IHttpActionResult> PutAgreedPrivacy(int id, EmployeeDto employeeDto)
	    {
		    var authenticatedEmployee = await GetAuthenticatedEmployee(false, false);
		    if (!authenticatedEmployee.IsAuthenticated)
			    return BadRequest(authenticatedEmployee.ErrorMessage);

		    try
		    {
			    if (authenticatedEmployee.Employee.Id != id)
				    return BadRequest("The employee you're attempting to update isn't the authenticated employee");

			    var existingEmployee = await Db.Employees.FindAsync(authenticatedEmployee.Employee.Id);
			    if (existingEmployee == null)
				    return NotFound();

			    existingEmployee.LastSignedOn = DateTime.Now;
			    existingEmployee.HasAgreedPrivacy = employeeDto.HasAgreedPrivacy;

			    Db.Entry(existingEmployee).State = EntityState.Modified;

			    await Db.SaveChangesAsync();
		    }
		    catch (DbUpdateConcurrencyException)
		    {
			    return NotFound();
		    }
		    catch (Exception e)
		    {
			    Trace.WriteLine(e);
			    return BadRequest(e.Message);
		    }

		    return StatusCode(HttpStatusCode.NoContent);
	    }
	}
}