﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using Feedback.Common.Context;
using Feedback.Common.Extensions;
using Feedback.Common.Models;
using Feedback.Common.Models.Dto;
using Feedback.Models.PeopleFinder;
using Feedback.Services;

namespace Feedback.Authentication
{
    public abstract class AuthenticatedApiController : ApiController
    {
        internal readonly FeedbackContext Db = new FeedbackContext();

        private static readonly bool AuthenticationEnabled = ConfigurationManager.AppSettings.GetValue("EnableAuthentication", false);
        private readonly IPeopleFinderHttpService _peopleFinder;
	    private int _testEmployeeId = 1;

        protected AuthenticatedApiController()
        {
            _peopleFinder = new PeopleFinderHttpService();

			if (!AuthenticationEnabled)
				_testEmployeeId = ConfigurationManager.AppSettings.GetValue("TestEmployeeId", 1);

		}

        internal async Task<AuthenticatedEmployee> GetAuthenticatedEmployee(bool createOnNotFound = false, bool errorOnNotAgreed = true)
        {
            if (!AuthenticationEnabled)
            {
                if (Request.Headers.Contains("EmployeeId"))
                {
                    var headers = Request.Headers.FirstOrDefault(x => x.Key.Equals("EmployeeId")).Value;
                    if (headers != null)
                    {
                        foreach (string header in headers)
                        {
                            _testEmployeeId = int.Parse(header);
                            break;
                        }
                    }
                }

                var employee = await Db.Employees.FindAsync(_testEmployeeId);
	            return new AuthenticatedEmployee {Employee = employee, IsAuthenticated = true};
            }

            // Do we have a valid login?
            try
            {
                // Determine if we have a login starting with ntid or normal email
                //var email = "deg88s@bp365.bp.com";
                var email = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Upn).Value;

                if (string.IsNullOrWhiteSpace(email))
                    return null;

                int indexOfAt = email.IndexOf("@", StringComparison.CurrentCultureIgnoreCase);
                int indexOfPoint = email.IndexOf(".", StringComparison.CurrentCultureIgnoreCase);

                Employee employee;
                if (indexOfPoint > indexOfAt)
                {
                    string ntid = email.Substring(0, indexOfAt).ToUpper();

                    employee = await Db.Employees.FirstOrDefaultAsync(x => x.NtId == ntid);
                    if (employee != null)
                    {
						if (errorOnNotAgreed && (!employee.HasAgreedLegal || !employee.HasAgreedPrivacy))
							return new AuthenticatedEmployee { ErrorMessage = $"Employee {employee.NtId} has not agreed legal & privacy." };

						return new AuthenticatedEmployee { Employee = employee, IsAuthenticated = true };
					}

                    // Can we find the employee via people finder?
                    ByIdResult byIdResult = await _peopleFinder.GetById(ntid);
                    if (byIdResult?.Data == null || !byIdResult.Data.Any())
	                    return new AuthenticatedEmployee { ErrorMessage = $"Unable to retrieve Employee {ntid} from people finder." };

                    if (!createOnNotFound)
	                    return new AuthenticatedEmployee { ErrorMessage = $"Could not find Employee {ntid}." };

                    return new AuthenticatedEmployee { Employee = await AddEmployee(byIdResult.Data[0].Detail, true), IsAuthenticated = true }; 
                }

                // Fall back to trying email address
                employee = await Db.Employees.FirstOrDefaultAsync(x => x.Email == email);
                if (employee != null)
                {
	                if (errorOnNotAgreed && (!employee.HasAgreedLegal || !employee.HasAgreedPrivacy))
		                return new AuthenticatedEmployee { ErrorMessage = $"Employee {employee.NtId} has not agreed legal & privacy." };
					
					return new AuthenticatedEmployee { Employee = employee, IsAuthenticated = true };
				}

				ByNameResult byNameResult = await _peopleFinder.GetByEmail(email);
                if (byNameResult?.Data == null || !byNameResult.Data.Any())
	                return new AuthenticatedEmployee { ErrorMessage = $"Unable to retrieve Employee {email} from people finder." };

                if (!createOnNotFound)
	                return new AuthenticatedEmployee { ErrorMessage = $"Could not find Employee {email}." };
				
	            return new AuthenticatedEmployee { Employee = await AddEmployee(byNameResult.Data[0], true), IsAuthenticated = true};
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
	            return new AuthenticatedEmployee { ErrorMessage = $"Could not find Employee {e.Message}." };
            }
        }

        internal async Task<Employee> AddEmployee(Detail detail, bool isSigningIn)
        {
            var employee = new Employee();

            employee.Fullname = detail.AlternateFullName;
            employee.Email = detail.PrimaryEmail;
            employee.NtId = detail.PrimaryLogin.ToUpper();
            employee.HasPhoto = detail.HasPhoto;
            employee.Title = detail.Title;
            employee.PeopleFinderId = int.Parse(detail.EndUserId);
            employee.HasAgreedLegal = false;
            employee.HasAgreedPrivacy = false;
            employee.LastUpdatedFromPeopleFinder = DateTime.Now;
            employee.LastSignedOn = isSigningIn ? DateTime.Now : new DateTime(1970, 1, 1);
	        employee.NotifyOnFeedback = true;

			Db.Employees.Add(employee);
            await Db.SaveChangesAsync();
            return employee;
        }

        private async Task<Employee> AddEmployee(PersonDetail detail, bool isSigningIn)
        {
            var employee = new Employee();

            employee.Fullname = detail.FullName;
            employee.Email = detail.Email;
            employee.NtId = detail.Login.ToUpper();
            employee.HasPhoto = detail.HasPhoto;
            employee.Title = detail.Title;
            employee.PeopleFinderId = int.Parse(detail.Id);
            employee.HasAgreedLegal = false;
            employee.HasAgreedPrivacy = false;
            employee.LastUpdatedFromPeopleFinder = DateTime.Now;
            employee.LastSignedOn = isSigningIn ? DateTime.Now : new DateTime(1970, 1, 1);
	        employee.NotifyOnFeedback = true;

            Db.Employees.Add(employee);
            await Db.SaveChangesAsync();
            return employee;
        }

        internal PaginationDto AddPagination(int totalCount, int page, int pageSize, string routeName)
        {
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);

            var urlHelper = new UrlHelper(Request);
            var prevLink = page > 0 ? urlHelper.Link(routeName, new { page = page - 1, pageSize }) : "";
            var nextLink = page < totalPages - 1 ? urlHelper.Link(routeName, new { page = page + 1, pageSize }) : "";

            var paginationHeader = new PaginationDto()
            {
                TotalCount = totalCount,
                TotalPages = totalPages,
                PrevPageLink = prevLink,
                NextPageLink = nextLink
            };
            return paginationHeader;
            //System.Web.HttpContext.Current.Response.Headers.Add("X-Pagination", Newtonsoft.Json.JsonConvert.SerializeObject(paginationHeader));
        }
    }
}