﻿using System.Configuration;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Web.Http;
using Feedback.Common.Extensions;

namespace Feedback
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            // If authorisation is enabled via config turn on here
            if (ConfigurationManager.AppSettings.GetValue("EnableAuthentication", false))
            {
              Trace.WriteLine("Enabling global authentication on all controllers");
              config.Filters.Add(new AuthorizeAttribute());
            }
            else
            {
              Trace.WriteLine("Disabling global authentication on all controllers");
            }

            config.Routes.MapHttpRoute(
                      name: "DefaultApi",
                      routeTemplate: "api/{controller}/{id}",
                      defaults: new { id = RouteParameter.Optional }
                  );
        }
    }
}
