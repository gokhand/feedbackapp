﻿using System.Configuration;
using System.IdentityModel.Tokens;
using Feedback.Common.Extensions;
using Microsoft.Owin.Security.ActiveDirectory;
using Owin;

namespace Feedback
{
    public partial class Startup
    {
        private readonly bool _authenticationEnabled = ConfigurationManager.AppSettings.GetValue("EnableAuthentication", false);
        
        public void ConfigureAuth(IAppBuilder app)
        {
            if (!_authenticationEnabled)
                return;

            app.Use(async (context, next) =>
            {
                await next.Invoke();
            });

            app.UseWindowsAzureActiveDirectoryBearerAuthentication(
                new WindowsAzureActiveDirectoryBearerAuthenticationOptions
                {
                    Tenant = ConfigurationManager.AppSettings["ida:Tenant"],
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = ConfigurationManager.AppSettings["ida:Audience"]
                    }
                });

            app.Use(async (context, next) =>
                    {
                        await next.Invoke();
                    });

        }
    }
}
