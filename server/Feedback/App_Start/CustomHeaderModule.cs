﻿using System;
using System.Web;

namespace Feedback.App_Start
{
    // We remove Server from our response headers for security.
    // Don't advertise the server type hosting the application.
    public class CustomHeaderModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += Context_PreSendRequestHeaders;
        }

        private void Context_PreSendRequestHeaders(object sender, EventArgs e)
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Response.Headers.Remove("Server");
        }

        public void Dispose()
        {
        }
    }
}
