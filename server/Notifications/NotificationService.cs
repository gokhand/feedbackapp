﻿using System.Diagnostics;
using System.ServiceProcess;

namespace Feedback.Notifications
{
	partial class NotificationService : ServiceBase
	{
		public readonly NotificationProcess NotificationProcess = new NotificationProcess();

		public NotificationService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			Trace.WriteLine("Requesting 240 seconds to start");
			RequestAdditionalTime(1000 * 1000);
			NotificationProcess.Initialise();
		}

		protected override void OnStop()
		{
			Trace.WriteLine("Service Stopping", "INFO");
			NotificationProcess.Stop();
			Trace.WriteLine("Service Stopped", "INFO");
			ExitCode = 0;
		}

		#region Custom Commands
		/// <summary>
		/// Custom commands can be sent to the Scheduler by sending the custom commands via the <see cref="System.ServiceProcess.ServiceController">ServiceController</see> object.
		/// </summary>
		public enum SimpleServiceCustomCommands : int
		{
			/// <summary>
			/// Signal the worker thread to terminate. For this custom command, the main service continues to run without a worker thread.
			/// </summary>
			StopWorker = 128,

			/// <summary>
			/// Restart the worker thread if necessary.
			/// Rebuilds the working set and begins processing.
			/// </summary>
			RestartWorker,

			/// <summary>
			/// Pauses the service
			/// </summary>
			Pause,

			/// <summary>
			/// Lists events due to fire within the next 24 hour period. Info logging must be enabled to view the events.
			/// </summary>
			RollLog
		};

		public void CustomCommand(int command)
		{
			OnCustomCommand(command);
		}

		/// <summary>
		/// Executes custom command may be passed through command prompt or via the <see cref="System.ServiceProcess.ServiceController">ServiceController</see> object if service is running as Windows Service.
		/// </summary>
		/// <param name="command">Casted value of <see cref=SimpleServiceCustomCommands>SimpleServiceCustomCommands</see></param>
		protected override void OnCustomCommand(int command)
		{
			// If the custom command is recognized,
			// signal the worker thread appropriately.
			switch (command)
			{
				case (int)SimpleServiceCustomCommands.StopWorker:
					// Signal the worker thread to terminate.
					// For this custom command, the main service
					// continues to run without a worker thread.
					OnStop();
					break;

				case (int)SimpleServiceCustomCommands.RestartWorker:
					// Restart the worker thread if necessary.
					OnStart(null);
					break;

				case (int)SimpleServiceCustomCommands.Pause:
					NotificationProcess.Pause();
					break;

				// Iterate through all events due to fire and log them to we can see what the hell is going on 
				case (int)SimpleServiceCustomCommands.RollLog:
					NotificationProcess.RollLog();
					break;
			}
		}
		#endregion
	}
}
