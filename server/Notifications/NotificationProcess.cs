﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Feedback.Common.Context;
using Feedback.Common.Diagnostics;
using Feedback.Common.Extensions;
using Feedback.Common.Models;
using log4net;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Feedback.Notifications
{
	public class NotificationProcess
	{
		private readonly FeedbackContext _db;

		private Timer _eventTimer;
		private readonly int _runAtHour;
		private readonly int _refreshRateMilliSeconds;
		private readonly int _throttleMilliseconds = ConfigurationManager.AppSettings.GetValue("ThrottleMilliseconds", 500);
		private readonly string _sendgridApiKey = ConfigurationManager.AppSettings.GetValue("SendgridApiKey", "");
		private readonly string _website = ConfigurationManager.AppSettings.GetValue("ida:Audience", "");

		private readonly Setting _setting;

		public NotificationProcess()
		{
			TraceExtensions.Info("Starting...");

			try
			{
				TraceExtensions.Info($"CurrentCulture is {CultureInfo.CurrentCulture.Name}.");

				_db = new FeedbackContext();

				_runAtHour = ConfigurationManager.AppSettings.GetValue("RunAtHour", 7);
				if (ConfigurationManager.AppSettings.GetValue("ConsoleDebug", false))
				{
					// Testing check every 10 seconds
					_refreshRateMilliSeconds = ConfigurationManager.AppSettings.GetValue("RefreshRateHours", 24) * 10000;
				}
				else
				{
					_refreshRateMilliSeconds = ConfigurationManager.AppSettings.GetValue("RefreshRateHours", 24) * 3600 * 1000;
				}

				// Get our starting date time
				_setting = _db.Settings.Find(1);

				TraceExtensions.Info("Started");
			}
			catch (Exception e)
			{
				Environment.ExitCode = 1;
				TraceExtensions.Warn("Hard fail");
				Trace.Fail(e.ToString());
			}
		}

		public void Initialise()
		{
			try
			{
				TraceExtensions.Info("Initialising...");
				IsPaused = false;

				TimeToProduceNotifications(null);
				TraceExtensions.Info("Initialised");
			}
			catch (Exception exception)
			{
				// We need to hard fail if no db available
				Environment.ExitCode = 1;
				TraceExtensions.Warn("Hard fail");
				Trace.Fail(exception.ToString());
			}
		}

		private void EnableTimer()
		{
			_eventTimer?.Dispose();

			if (IsPaused)
				return;

			if (IsProcessing)
				return;

			TraceExtensions.Info($"Will check need to produce notifcations again at {DateTime.Now.AddMilliseconds(_refreshRateMilliSeconds)}.");

			_eventTimer = new Timer(TimeToProduceNotifications, null, _refreshRateMilliSeconds, Timeout.Infinite);
		}

		private async void TimeToProduceNotifications(object state)
		{
			if (IsPaused)
			{
				TraceExtensions.Info("Firing of events is paused, send play to service to re-enable processing.");
				return;
			}

			if (IsProcessing)
			{
				TraceExtensions.Info("Firing of events is already in motion. Why are we re-entering this method?");
				return;
			}

			_eventTimer?.Dispose();

			try
			{
				IsProcessing = true;

				TraceExtensions.Info($"Checking if time to produce notifications, Last Produced at: {_setting.DateTime}");

				DateTime checkedAt = DateTime.Now.Date.AddHours(_runAtHour);

				// Get last time reports were run.
				DateTime lastRun = _setting.DateTime;

				if (checkedAt <= lastRun)
				{
					TraceExtensions.Info("Not time to produce notifications");
					return;
				}

				if (lastRun.AddDays(1) > checkedAt)
				{
					TraceExtensions.Info("Not time to produce notifications");
					return;
				}

				// Ok let's run the daily notifications.
				DateTime runDateTime = lastRun.AddDays(1);

				TraceExtensions.Info($"Producing notifications for: {runDateTime}");
				await ProduceNotifications(runDateTime);
				TraceExtensions.Info($"Completed Producing notifications for: {runDateTime}");

				try
				{
					_setting.DateTime= runDateTime;
					_db.Entry(_setting).State = EntityState.Modified;
					await _db.SaveChangesAsync();
				}
				catch (Exception ex)
				{
					TraceExtensions.Error(ex.Message);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
			}
			finally
			{
				IsProcessing = false;
				EnableTimer();
			}
		}

		private async Task ProduceNotifications(DateTime lastNotificationDateTime)
		{
			try
			{
				// Retrieve all threads which have content newer than our last notification date
				// Get instigating and receiving employees and union so employess only receive a single email regardless of activity type.
				IQueryable<Employee> instigatingEmployees = from t in _db.Threads
					where t.LastUpdated > lastNotificationDateTime
					select t.InstigatingEmployee;

				IQueryable<Employee> receivingEmployees = from t in _db.Threads
					where t.LastUpdated > lastNotificationDateTime
					select t.ReceivingEmployee;

				List<Employee> employeesWithActivity = await instigatingEmployees.Union(receivingEmployees).ToListAsync();

				if (!employeesWithActivity.Any())
				{
					TraceExtensions.Info("No feedback activity to notify.");
					return;
				}

				TraceExtensions.Info($"Sending {employeesWithActivity.Count} notifications.");

				var sendGridClient = new SendGridClient(_sendgridApiKey);
				var fromEmail = new EmailAddress(_setting.From);
				string subject = _setting.Subject;

				foreach (var employee in employeesWithActivity)
				{
					string content = _setting.Content;
					content = content.Replace("{username}", employee.Fullname);
					content = content.Replace("{website}", _website);

					var msg = new SendGridMessage
					{
						From = fromEmail,
						Subject = subject,
						HtmlContent = content
					};
					msg.AddTo(new EmailAddress(employee.Email, employee.Fullname));
					
					Response response = await sendGridClient.SendEmailAsync(msg);

					if (response.StatusCode != HttpStatusCode.OK)
						TraceExtensions.Error($"{response.StatusCode} - {response.Headers} - {msg.Serialize()}");

					// Throttle how quickly we're sending emails
					await Task.Delay(_throttleMilliseconds);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString());
			}   
		}

		public void Stop()
		{
			TraceExtensions.Warn("Stopping Process");
			_eventTimer?.Dispose();
			TraceExtensions.Warn("Stopped Process");
		}

		public void Pause()
		{
			TraceExtensions.Warn("Pausing Process");
			IsPaused = !IsPaused;
			TraceExtensions.Warn("Paused Process");
		}

		private bool _isProcessing = false;
		private readonly object _isProcessingLock = new object();

		public bool IsProcessing
		{
			get
			{
				lock (_isProcessingLock)
					return _isProcessing;
			}
			set
			{
				lock (_isProcessingLock)
					_isProcessing = value;
			}
		}

		private bool _isPaused;
		private readonly object _isPausedLock = new object();
		public bool IsPaused
		{
			get
			{
				lock (_isPausedLock)
					return _isPaused;
			}
			set
			{
				lock (_isPausedLock)
					_isPaused = value;
			}
		}

		public void RollLog()
		{
			var repositories = LogManager.GetAllRepositories();
			if (repositories.Any())
			{
				var appenders = repositories[0].GetAppenders();
				var appender = appenders.FirstOrDefault(x => x.GetType() == typeof(ForceableRollingFileAppender));
				if (appender != null)
				{
					TraceExtensions.Info($"Forcing roll of log '{appender.Name}'");
					((ForceableRollingFileAppender)appender).PreserveLogFileNameExtension = true;
					((ForceableRollingFileAppender)appender).Roll();
					((ForceableRollingFileAppender)appender).PreserveLogFileNameExtension = false;
					TraceExtensions.Info("Forced roll completed");
				}
			}
		}
	}
}
