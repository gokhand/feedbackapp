﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using Feedback.Common.Diagnostics;
using Feedback.Common.Extensions;
using Topshelf;
using System.Linq;

namespace Feedback.Notifications
{
	class Program
	{
		public static NotificationService NotificationService;

		static void Main(string[] args)
		{
			bool isConsole = HasCommand("c", args);
			bool isCommand = HasCommand(new[] { "p", "f", "g", "s", "q" }, args);

			try
			{
				Trace.Listeners.Add(new ConsoleTraceListener());
				Trace.Listeners.Add(new Log4netTraceListener());

				if (isConsole)
				{
					try
					{
						NotificationService = new NotificationService();

						Console.BufferHeight = Int16.MaxValue - 1;
						Console.BufferWidth = 200;
						Console.WindowWidth = 200;
						Console.WindowHeight = 50;
					}
					finally
					{
						RunOnConsole(NotificationService);
					}
					return;
				}

				if (isCommand)
				{
					ServiceController sc = new ServiceController("BPHRFeedback");

					if (HasCommand("p", args))
					{
						sc.ExecuteCommand((int)NotificationService.SimpleServiceCustomCommands.Pause);
						return;
					}

					if (HasCommand("f", args))
					{
						sc.ExecuteCommand((int)NotificationService.SimpleServiceCustomCommands.RollLog);
						return;
					}

					if (HasCommand("g", args))
					{
						sc.ExecuteCommand((int)NotificationService.SimpleServiceCustomCommands.RestartWorker);
						return;
					}

					if (HasCommand(new[] { "g", "s" }, args))
					{
						sc.ExecuteCommand((int)NotificationService.SimpleServiceCustomCommands.StopWorker);
						return;
					}

					return;
				}

				// Runs as a service
				HostFactory.Run(x =>
				{
					x.Service<NotificationService>(s =>
					{
						s.ConstructUsing(name => new NotificationService());
						s.WhenStarted(sws => sws.NotificationProcess.Initialise());
						s.WhenStopped(sws => sws.NotificationProcess.Stop());
						s.WhenCustomCommandReceived((service, control, command) => service.CustomCommand(command));
					});

					x.StartAutomaticallyDelayed();
					x.RunAsNetworkService();
					x.EnableShutdown();

					x.SetDescription("BP HR Feedback Notifications");
					x.SetDisplayName("BP HR Feedback");
					x.SetServiceName("BPHRFeedback");
				});
			}
			catch (Exception e)
			{
				Trace.Fail("Service exited with exception" + e);
				NotificationService?.Stop();
			}
		}

		private static bool HasCommand(string command, string[] args)
		{
			return HasCommand(new[] { command }, args);
		}

		private static bool HasCommand(string[] command, string[] args)
		{
			foreach (string s in args)
			{
				var arg = s.Replace("/", "");
				arg = arg.Replace("-", "");
				arg = arg.ToLower();

				if (command.Contains(arg))
					return true;
			}
			return false;
		}

		/// <summary>
		/// Make it easy to debug by running on the console, use command line arg /c to do this.
		/// </summary>
		/// <param name="cmdConsole">The service to run in console mode.</param>
		public static void RunOnConsole(NotificationService cmdConsole)
		{
			// Simulate service control event loop - only allow service to
			// start and stop manually in the console
			bool finished = false;
			char keyState = 'g';
			while (!finished)
			{
				switch (keyState)
				{
					case 'g':
						TraceExtensions.Info("Starting...");
						cmdConsole.NotificationProcess.Initialise();
						break;
					case 's':
					case 'q':
						TraceExtensions.Info("Stopping...");
						cmdConsole.NotificationProcess.Stop();
						if (keyState == 'q') finished = true;
						break;
					case 'p':
						cmdConsole.NotificationProcess.Pause();
						break;
					case 'f':
						cmdConsole.NotificationProcess.RollLog();
						break;
				}
				if (!finished)
				{
					TraceExtensions.Info("Press S(top), G(o), Q(uit), P(ause), F(orce log roll)");
					// this blocks
					keyState = Console.ReadKey().KeyChar;
					Console.WriteLine("");
				}
			}
		}
	}
}
