# BP HR Feedback app

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.6.
---
## DO's and DONT's
DO CHECK  if you have the correct version of Node (v7+) and Npm (v5+)

DO COMPLY `.editorconfig`

DO CONFIGURE Your IDE to respect projects lint rules

DO USE ng cli for generating components and modules. Components should have a single responsibility, and have least amount of dependencies to function

DO NOT USE `sudo` to run `npm install`

## Setup

Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Dev server with SSL for ADAL

Run `ng serve --ssl true --port 44666`

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build --env=prod -aot --target=production` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Environments

API url's are defined in environments folder, and configured in angular-cli.json.

Application specific configuration is in app.config. For a spefic build use `ng build --env=test -aot --target=production`

Target flag does further optimization

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
