import { BPFEEDBACKAPPPage } from './app.po';

describe('bp-feedback-app App', () => {
  let page: BPFEEDBACKAPPPage;

  beforeEach(() => {
    page = new BPFEEDBACKAPPPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
